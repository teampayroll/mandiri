<?php
	if(!isset($_SESSION)){session_start();}
	ob_start();

	include("_fileinclude.php");

	unset($_SESSION['id']);
	unset($_SESSION['admin']);
	unset($_SESSION['email']);
	unset($_SESSION['type']);

	// this makes the time to the current time
	// 10 seconds
	$past = time() - 10;
	setcookie('id', '', $past);
	setcookie('admin', '', $past);
	setcookie('email', '', $past);
	setcookie('type', '', $past);
	historyActivity($adminId, 1, 0, $datepost, $ipaddresspost);
	redirectSite('index.php');
	exit;

	ob_flush();
?>