<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "historyactivity";
    $subpage = "";
    $page_name = "History Activity";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>
    <?php
      $pnum=1;
      if(isset($_GET['pnum']))
      {
        $pnum = securefield($_GET['pnum'],5);
      }

      $statusGet ="";
      $nameGet ="";
      $dateStartGet ="";
      $dateEndGet = "";


      $sqlFilter = 1;
      if(isset($_GET['action_activity'])){
        //ACTIVITY
        if($_GET['action_activity'] != "" && is_numeric($_GET['action_activity']) ){
          $statusGet = $_GET['action_activity'];
          $sqlFilter .= " AND a.type = $statusGet";   
        }
      }
      //Date
      if(isset($_GET['dari_otorisasi'])){
        if($_GET['dari_otorisasi'] != ""){
          
          $dateStartGet =  $_GET['dari_otorisasi'] ;
          $dateEndGet = $_GET['sampai_otorisasi'] ;

          $dateStart = substr($dateStartGet,6,4) . "/" . substr($dateStartGet,3,2) . "/" . substr($dateStartGet,0,2);
          $dateEnd = substr($dateEndGet,6,4) . "/" . substr($dateEndGet,3,2) . "/" . substr($dateEndGet,0,2);
         
          $sqlFilter .=  " AND a.updateddate BETWEEN '$dateStart' AND DATE_ADD('$dateEnd', INTERVAL 1 DAY)";
        }
      }
      if(isset($_GET['user_otorisasi'])){
       if($_GET['user_otorisasi'] != ""){
          $nameGet = $_GET['user_otorisasi'];
          $sqlFilter .= " AND b.name LIKE '%$nameGet%'";
       }
      }

     // set default pagination
      $recordperpages = 10;
      $targetpage = "history_activity.php?action_activity=$statusGet&user_otorisasi=$nameGet&dari_otorisasi=$dateStartGet&sampai_otorisasi=$dateEndGet";

      $startpages = ($pnum-1)*$recordperpages;

      $query = "SELECT a.id, b.name, a.type, a.idrelated, a.updateddate , 
      c.filecsv as csvschedule, d.filecsv as csvbulk, c.infosubmitter as commentschedule, d.infosubmitter as commentbulk,
      
      c.idapprover_a as idapproveraschedule, c.commentapprover_a as commentapproveraschedule,
      c.idapprover_b as idapproverbschedule, c.commentapprover_b as commentapproverbschedule,

      d.idapprover_a as idapproverabulk, d.commentapprover_a as commentapproverabulk,
      d.idapprover_b as idapproverbbulk, d.commentapprover_b as commentapproverbbulk

      FROM mst_history_activity a 
      INNER JOIN mst_user b ON a.iduser = b.id
      LEFT JOIN mst_schedule c ON  a.idrelated = c.id 
      LEFT JOIN mst_update_member d ON  a.idrelated = d.id 
      WHERE $sqlFilter
      ORDER BY a.id desc";

      $result = mysqlQuery($query);
      $totalrecords = mysqlNumRows($result);
      $query = $query." LIMIT $startpages,$recordperpages";

    ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        
        <?php include('_nav.php'); ?>

        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                History Activity
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
                <form>
                  <div class="row clearfix">
                    <div class="col-xs-12 mt_40">
                      <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <label for="action_otorisasi" class="w_normal">Aktifitas</label>
                            <select class="form-control br_0 c_grey" id="action_activity" name="action_activity">
                              <option <?php if($statusGet == ""){ echo 'selected';}?> value="">Semua</option>
                              <option <?php if($statusGet == "0"){ echo 'selected';}?> value="0">Login</option>
                              <option <?php if($statusGet == "1"){ echo 'selected';}?> value="1">Logout</option>
                              <option <?php if($statusGet == "2"){ echo 'selected';}?> value="2">One to Many Transfer</option>
                              <option <?php if($statusGet == "3"){ echo 'selected';}?> value="3">Bulk Member</option>
                              <option <?php if($statusGet == "4"){ echo 'selected';}?> value="4">Approval One to Many</option>
                              <option <?php if($statusGet == "5"){ echo 'selected';}?> value="5">Approval Bluk Member</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <label for="user_activity" class="w_normal">Nama User</label>
                            <input type="text" class="form-control br_0 c_grey" id="user_otorisasi" name="user_otorisasi" placeholder="" value="<?php echo $nameGet?>">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                          <label for="dari_otorisasi" class="w_normal">Dari Tanggal</label>
                          <div class='input-group date' id='datetimepicker1'>
                              <input type='text' class="form-control br_0 c_grey bg_white cs_pt" id="dari_otorisasi" name="dari_otorisasi" value="<?php echo $dateStartGet?>" readonly="readonly" />
                              <span id="wrap_icon_date" class="input-group-addon cs_df">
                                  <img src="<?php echo siteUrl(); ?>images/icon_date.png" />
                                  <span class="italic c_yellow fs_12">(dd/mm/yyyy)</span>
                              </span>
                          </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                          <label for="sampai_otorisasi" class="w_normal">Sampai Tanggal</label>
                          <div class='input-group date' id='datetimepicker1'>
                              <input type='text' class="form-control br_0 c_grey bg_white cs_pt" id="sampai_otorisasi" name="sampai_otorisasi" value="<?php echo $dateEndGet?>" readonly="readonly" />
                              <span id="wrap_icon_date" class="input-group-addon cs_df">
                                  <img src="<?php echo siteUrl(); ?>images/icon_date.png" />
                                  <span class="italic c_yellow fs_12">(dd/mm/yyyy)</span>
                              </span>
                          </div>
                      </div>
                    </div>
                    <div class="col-xs-12 mb_40">
                      <div class="right">
                        <button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Cari</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>

            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Hasil Pencarian
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mb_40">
              <div class="bg_white c_blue fs_14">
                <div class="table-responsive">
                  <table id="wrap_table" class="table">
                    <thead>
                      <tr>
                        <th>Tanggal & Jam</th>
                        <th>Nama User</th>
                        <th>Aktifitas</th>
                        <th>Comment</th>
                        <th>Imported File</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          $result = mysqlQuery($query);
                          if(mysqlNumRows($result)){
                            while($row = mysqlFetchArray($result)){
                              $id = trim($row['id']); 
                              $name = trim($row['name']);
                              $idrelated =  trim($row['idrelated']);
                              $type = trim($row['type']); 
                              $updateddate     = numDate(trim($row['updateddate']));
                              
                              if($type == 2){ // if schedule
                                 $filecsv = trim($row['csvschedule']); 
                                 $comment = trim($row['commentschedule']); 
                              }else if($type == 3){ // if bulk member
                                 $filecsv = trim($row['csvbulk']);
                                 $comment = trim($row['commentbulk']); 
                              }else if($type == 4){ // if schedule approvar
                                 $filecsv = trim($row['csvschedule']); 
                                 $idapproveraschedule = $row['idapproveraschedule'];
                                 if( $idapproveraschedule != 0){ // approver a
                                  $comment = trim($row['commentapproveraschedule']); 
                                  }else{  //approver b
                                  $comment = trim($row['commentapproverbschedule']);  
                                  }
                              }else if($type == 5){ // if bulk approval
                                 $filecsv = trim($row['csvbulk']);
                                 $idapproverabulk = $row['idapproverabulk'];
                                 if( $idapproverabulk != 0){ // approver a
                                  $comment = trim($row['commentapproverabulk']); 
                                  }else{ //approver b
                                  $comment = trim($row['commentapproverbbulk']);  
                                  }
                                 
                              }else{
                                $filecsv = "";
                                $comment = "";
                              }
                               
                        ?>
                      <tr>
                        <td><?php echo $updateddate ?></td>
                        <td><?php echo $name ?></td>
                        <td>
                          <?php
                            switch($type){
                              case 0:
                                echo 'Login';
                                break;
                              case 1:
                                echo 'Logout';
                                break;
                              case 2:
                                echo 'One to Many';
                                break;
                              case 3:
                                echo 'Bulk Member';
                                break;
                              case 4:
                                echo 'Approval';
                                break;
                              case 5:
                                echo 'Approval';
                                break;

                            }


                          ?>

                        </td>
                        <td> <?php echo $comment?> </td>
                        <td> <?php echo $filecsv?></td>
                        <td class="action">
                          <a href="detail_activity.php?id=<?php echo $id ?>"><img src="<?php echo siteUrl(); ?>images/icon_edit.png" /></a>

                        </td> 
                      </tr>
                      <?php
                          }
                        }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mb_40">
              <div id="wrap_page" class="right cs_df">
                <?php
                  echo generatePagination($totalrecords, $recordperpages, $targetpage, $pnum, "pnum", 2);
                ?>
              </div>
            </div>
          </div>
        </div>

        <?php include( PLUGPATH ."_footer.php"); ?>
        
      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
</html>
<?php ob_flush(); ?>