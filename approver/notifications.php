<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "notifications";
    $subpage = "";
    $page_name = "Notifications";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        
        <?php include('_nav.php'); ?>

        </div>
        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Notifications
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
                <form>
                  <div class="row clearfix">
                    <div class="col-xs-12 mt_40">
                      <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <label for="action_notif" class="w_normal">Aktifitas</label>
                            <select class="form-control br_0 c_grey" id="action_notif" name="action_notif">
                              <option>Semua</option>
                              <option>Upload</option>
                              <option>Approve</option>
                              <option>Release</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <label for="user_notif" class="w_normal">Nama User</label>
                            <input type="text" class="form-control br_0 c_grey" id="user_notif" name="user_notif" placeholder="">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                          <label for="dari_notif" class="w_normal">Dari Tanggal</label>
                          <div class='input-group date' id='datetimepicker1'>
                              <input type='text' class="form-control br_0 c_grey bg_white cs_pt" id="dari_notif" name="dari_notif" readonly="readonly" />
                              <span id="wrap_icon_date" class="input-group-addon cs_df">
                                  <img src="<?php echo siteUrl(); ?>images/icon_date.png" />
                                  <span class="italic c_yellow fs_12">(dd/mm/yyyy)</span>
                              </span>
                          </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                          <label for="sampai_notif" class="w_normal">Sampai Tanggal</label>
                          <div class='input-group date' id='datetimepicker1'>
                              <input type='text' class="form-control br_0 c_grey bg_white cs_pt" id="sampai_notif" name="sampai_notif" readonly="readonly" />
                              <span id="wrap_icon_date" class="input-group-addon cs_df">
                                  <img src="<?php echo siteUrl(); ?>images/icon_date.png" />
                                  <span class="italic c_yellow fs_12">(dd/mm/yyyy)</span>
                              </span>
                          </div>
                      </div>
                    </div>
                    <div class="col-xs-12 mb_40">
                      <div class="right">
                        <button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Cari</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>

            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Hasil Pencarian
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mb_40">
              <div class="bg_white c_blue fs_14">
                <div class="table-responsive">
                  <table id="wrap_table" class="table">
                    <thead>
                      <tr>
                        <th>Tanggal & Jam</th>
                        <th>Nama User</th>
                        <th>Aktifitas</th>
                        <th>Imported File</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>24/07/2014 | 20:00:03</td>
                        <td>Katy Perry</td>
                        <td>Upload</td>
                        <td>APF.CSV</td>
                        <td>
                          <a href=""><img src="<?php echo siteUrl(); ?>images/icon_edit.png" /></a>
                        </td>
                      </tr>
                      <tr>
                        <td>24/07/2014 | 20:00:03</td>
                        <td>Katy Perry</td>
                        <td>Approve</td>
                        <td>APF.CSV</td>
                        <td>
                          <a href=""><img src="<?php echo siteUrl(); ?>images/icon_edit.png" /></a>
                        </td>
                      </tr>
                      <tr>
                        <td>24/07/2014 | 20:00:03</td>
                        <td>Katy Perry</td>
                        <td>Release</td>
                        <td>APF.CSV</td>
                        <td>
                          <a href=""><img src="<?php echo siteUrl(); ?>images/icon_edit.png" /></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

        <?php include( PLUGPATH ."_footer.php"); ?>
        
      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
</html>