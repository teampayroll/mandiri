<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "historyotorisasi";
    $subpage = "detail";
    $page_name = "History Otorisasi";
?>
<!DOCTYPE html>
<html lang="en">
  <head>    
    <?php include( PLUGPATH ."_head.php"); ?>
	<?php
		$id = "";
		
		$user_name = "";		
		$noreftransaction = "";
		$total = "0";
		$jenistransaction = "";
		$totalrecord = "";
		$tanggalpembayaran = "";
		$filecsv = "";		
		$infosubmitter = "";
		$type = "";
		$status = "";
		$createddate = "";		
		
		$idapprover_a = "";
		$otp_code_a = "";
		$commentapprover_a = "";
		$status_approver_a = "";
		
		$idapprover_b = "";
		$otp_code_b = "";
		$commentapprover_b = "";
		$status_approver_b = "";
		
		if( getExist('id') && !equalGetStr('id','') ) {
			$id = secureLimit($_GET["id"], 11);
			$query = "  SELECT b.name, a.noreftransaction, a.total, a.jenistransaction, a.totalrecord, a.tanggalpembayaran, a.filecsv, a.infosubmitter, a.type,
						(CASE a.status
							WHEN '0' THEN 'Pending' 
							WHEN '3' THEN 'Approved'
							WHEN '4' THEN 'Denied'
							ELSE ''
						END) AS activity, a.createddate,
						a.idapprover_a, a.otp_code_a, a.commentapprover_a, a.status_approver_a,
						a.idapprover_b, a.otp_code_b, a.commentapprover_b, a.status_approver_b
						FROM mst_schedule a
						INNER JOIN mst_user b ON a.iduser = b.id AND b.status <> 2
						WHERE a.status <> 2 AND a.id = '$id'";
			$result = mysqlQuery($query);
			if(mysqlNumRows($result)){
				$row = mysqlFetchArray($result);
				$user_name         = trim($row['name']);
				$noreftransaction  = trim($row['noreftransaction']);
				$total      	   = trim($row['total']);
				$jenistransaction  = trim($row['jenistransaction']);
				$totalrecord       = trim($row['totalrecord']);
				$tanggalpembayaran = trim($row['tanggalpembayaran']);
				$filecsv   		   = trim($row['filecsv']);
				$infosubmitter     = trim($row['infosubmitter']);
					if ($infosubmitter!="") {$infosubmitter=str_replace("\n","<br />",$infosubmitter);}
				$type    		   = trim($row['type']);
				$status       	   = trim($row['activity']);
				$createddate       = trim($row['createddate']);
				
				$idapprover_a      = trim($row['idapprover_a']);
				$otp_code_a        = trim($row['otp_code_a']);
				$commentapprover_a = trim($row['commentapprover_a']);
					if ($commentapprover_a!="") {$commentapprover_a=str_replace("\n","<br />",$commentapprover_a);}
				$status_approver_a = trim($row['status_approver_a']);
				
				$idapprover_b      = trim($row['idapprover_b']);
				$otp_code_b        = trim($row['otp_code_b']);
				$commentapprover_b = trim($row['commentapprover_b']);
					if ($commentapprover_b!="") {$commentapprover_b=str_replace("\n","<br />",$commentapprover_b);}
				$status_approver_b = trim($row['status_approver_b']);
			}mysqlFreeResult($result);			
		}
		
		// delete this if session header activated -- for TESTING ONLY
		if (!isset($adminId)) {$adminId="3";}
		
		// get team type (as user login)
		$teamtype = "0";
		$query = "  SELECT team
					FROM mst_user
					WHERE status <> 2 AND id = '$adminId'";
		$result = mysqlQuery($query);
		if(mysqlNumRows($result)){
			$row = mysqlFetchArray($result);
			$teamtype = trim($row['team']);
		}mysqlFreeResult($result);
		
		// check if form has been approved/denied (as user login) - hidden OTP form & submit button
		$form_disable = "0";
		$msg_comment = "";
		if ($teamtype=="1") {if($status_approver_a!="0"){$form_disable="1";} $msg_comment = $commentapprover_a;}
		if ($teamtype=="2") {if($status_approver_b!="0"){$form_disable="1";} $msg_comment = $commentapprover_b;}
		
	?>
  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        
        <?php include('_nav.php'); ?>

        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Rincian Transaksi
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mb_40">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
                <!--<form>-->
                  <div class="row clearfix">
                    <div class="col-xs-12 mt_40">
                      <div class="form-group">
                        <label for="waktu_transaksi" class="w_normal">Waktu Transaksi</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="waktu_transaksi" name="waktu_transaksi" readonly="readonly" value="<?php echo numDate($createddate,9); ?>">
                      </div>
                      <div class="form-group">
                        <label for="noref_transaksi" class="w_normal">No Ref Transaksi</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="noref_transaksi" name="noref_transaksi" readonly="readonly" value="<?php echo $noreftransaction; ?>">
                      </div>
                      <div class="form-group">
                        <label for="dari_transaksi" class="w_normal">Dari</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="dari_transaksi" name="dari_transaksi" readonly="readonly" value="<?php echo $user_name; ?>">
                      </div>
                      <div class="form-group">
                        <label for="jenis_transaksi" class="w_normal">Jenis Transaksi</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="jenis_transaksi" name="jenis_transaksi" readonly="readonly" value="<?php echo $jenistransaction; ?>">
                      </div>
                      <div class="form-group">
                        <label for="jumlah_transaksi" class="w_normal">Jumlah Total</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="jumlah_transaksi" name="jumlah_transaksi" readonly="readonly" value="<?php echo number_format($total,0,',','.'); ?>">
                      </div>
                      <div class="form-group">
                        <label for="tanggal_transaksi" class="w_normal">Tanggal Pembayaran</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="tanggal_transaksi" name="tanggal_transaksi" readonly="readonly" value="<?php echo numDate($tanggalpembayaran,9); ?>">
                      </div>
                      <div class="form-group">
                        <label for="record_transaksi" class="w_normal">Jumlah Record</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="record_transaksi" name="record_transaksi" readonly="readonly" value="<?php echo $totalrecord; ?>">
                      </div>
                      <div class="form-group">
                        <label for="file_transaksi" class="w_normal">Imported File</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="file_transaksi" name="file_transaksi" readonly="readonly" value="<?php echo $filecsv; ?>">
                      </div>
                      <div class="form-group">
                        <label for="status_transaksi" class="w_normal">Status</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="status_transaksi" name="status_transaksi" readonly="readonly" value="<?php echo $status; ?>">
                      </div>
                      <div class="form-group">
                        <label for="keterangan_transaksi" class="w_normal">Keterangan</label>
                        <!--<input type="text" class="form-control br_0 bg_white c_grey" id="keterangan_transaksi" name="keterangan_transaksi" readonly="readonly" value="<?php //echo $infosubmitter; ?>">-->
						<p style="font-size:11px;color:#000">
							<?php echo $infosubmitter; ?>
						</p>
                      </div>
					  
					  <!-- showup if schedule -> approved/denied -->
					  <?php if ($form_disable=="1" && $msg_comment!='') { ?>
						  <div class="form-group">
							<label class="w_normal">Comments</label>
							<p style="font-size:11px;color:#000">
								<?php echo $msg_comment; ?>
							</p>
						  </div>
					  <?php } ?>
                    </div>
					
					<?php if ($form_disable=="0") { ?>
						<div class="col-xs-12 mb_40">
						  <div class="right">
							<button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20" onClick="sendotpemail()">Generate OTP to Email</button>
						  </div>
						</div>
					<?php } else { ?>
						<div class="col-xs-12 mb_40">
						  <div class="left">
							<button type="button" id="button_cancel" class="btn btn-default button_white fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20" onclick="self.location='history_otorisasi.php';">Kembali</button>
						  </div>
						</div>
					<?php } ?>
                  </div>
                <!--</form>-->
              </div>
			  <?php if ($form_disable=="0") { ?>
				  <div class="bg_white pl_20 pr_20 c_blue fs_14" style="border-top:1px solid #0f2b5b;">
					<!-- <form> -->
					  <div class="row clearfix">
						<div class="col-xs-12 mt_40">
						  <div class="form-group">
							<label for="code_otp" class="w_normal">OTP</label>
							<input type="text" class="form-control br_0 bg_white c_grey" id="code_otp" name="code_otp" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="">
						  </div>
						  <div class="form-group">
							<label for="comments_transaksi" class="w_normal">Comments</label>
							<textarea class="form-control br_0 textarea_input c_grey" id="comments_transaksi" name="comments_transaksi"></textarea>
						  </div>
						</div>
						<div class="col-xs-12 col-sm-4 mb_40">
							<button type="button" id="button_cancel" class="btn btn-default button_white fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20" onclick="self.location='history_otorisasi.php';">Kembali</button>
						</div>
						<div class="col-xs-12 col-sm-8 mb_40">
						  <div class="right">
							<button type="button" id="button_cancel" class="btn btn-default button_white fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20" onClick="approved_denied(2)">Tolak</button>
							<!--<button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20" data-toggle="modal" data-target="#cancel">Otorisasi</button>-->
							<button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20" onClick="approved_denied(1)">Otorisasi</button>
						  </div>
						  
						  <!-- popup info -->
						  <div class="modal fade" id="cancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog modal-sm" role="document">
							  <div class="modal-content">
								<div class="modal-header">
								  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								</div>
								<div id="modal-body" class="modal-body">
									
								</div>
								<div class="modal-footer">
								  <button type="button" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20" data-dismiss="modal">Ok</button>
								</div>
							  </div>
							</div>
						  </div>
						  
						</div>
					  </div>
					<!-- </form> -->
				  </div>
			  <?php } ?>
            </div>
          </div>
        </div>

        <?php include( PLUGPATH ."_footer.php"); ?>

      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
	<script language="javascript">
		function sendotpemail(){
			$.ajax({
				type: 'POST', // POST|GET
				url: "ajax_sendotpemail.php",
				data: { idschedule:<?php echo $id; ?> }, 
				//dataType: "html", // xml|json|script|html						
				success: function(data) {
					document.getElementById('modal-body').innerHTML = 'Email kode otorisasi sudah dikirimkan, silahkan cek Inbox email Anda.';
					$('#cancel').modal('show');
				},
				error: function(result) {
					// nothing error here
				} 
			});
		}	

		function approved_denied(pType){ // (1:approved | 2:denied)
			if (document.getElementById('code_otp').value=="")
			{			
				document.getElementById('modal-body').innerHTML = 'Masukkan kode OTP dahulu.';
				$('#cancel').modal('show');
				return false;
			}
			
			if (pType=='2') {
				if (document.getElementById('comments_transaksi').value=="")
				{			
					document.getElementById('modal-body').innerHTML = 'Masukkan komentar dahulu.';
					$('#cancel').modal('show');
					return false;
				}
			}
			
			$.ajax({
				type: 'POST', // POST|GET
				url: "ajax_approvaldenied.php",
				data: { 
					type:pType,
					idschedule:<?php echo $id; ?>,
					paydate:'<?php echo $tanggalpembayaran; ?>',
					totalpay:<?php echo $total; ?>,
					csvfile:'<?php echo $filecsv; ?>',
					otp:document.getElementById('code_otp').value,
					comment:document.getElementById('comments_transaksi').value 
				}, 
				//dataType: "html", // xml|json|script|html						
				success: function(data) {
					//alert(data);
					if (data=='otp-valid') { // valid OTP
						if (pType=='1') {document.getElementById('modal-body').innerHTML = 'Approval Anda sudah disimpan.';}
						if (pType=='2') {document.getElementById('modal-body').innerHTML = 'Penolakan Anda sudah disimpan.';}
						$('#cancel').modal('show');
						$("#cancel").on("hidden.bs.modal", function () {
							self.location='history_otorisasi.php';
						});	
					} else {
						if (data=='time-invalid') { // invalid time
							document.getElementById('modal-body').innerHTML = 'Maaf schedule yang akan anda approve sudah melebihi batas waktu.';
							$('#cancel').modal('show');
							$("#cancel").on("hidden.bs.modal", function () {
								self.location='history_otorisasi.php';
							});							
						} else {
							if (data=='total-invalid') { // invalid total
								document.getElementById('modal-body').innerHTML = 'Jumlah total berbeda dengan file CSV.';
								$('#cancel').modal('show');
								return false;
							} else {
								document.getElementById('modal-body').innerHTML = 'Kode OTP yang Anda masukkan salah.';
								$('#cancel').modal('show');
								return false;
							}
						}
					}
				},
				error: function(result) {
					// nothing error here
				} 
			});
		}		
	</script>
  </body>
</html>
<?php ob_flush(); ?>