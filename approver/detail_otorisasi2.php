<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "historyactivity";
    $subpage = "detail";
    $page_name = "History Activity";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        
        <?php include('_nav.php'); ?>

        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Rincian Transaksi
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mb_40">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
                <form method="post" action="http://yoai.vemine.com/download">
                  <div class="row clearfix">
                    <div class="col-xs-12 mt_40">
                      <div class="form-group">
                        <label for="waktu_transaksi" class="w_normal">Waktu Transaksi</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="waktu_transaksi" name="waktu_transaksi" readonly="readonly" value="20/05/2014 17:30:01">
                      </div>
                      <div class="form-group">
                        <label for="noref_transaksi" class="w_normal">No Ref Transaksi</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="noref_transaksi" name="noref_transaksi" readonly="readonly" value="">
                      </div>
                      <div class="form-group">
                        <label for="dari_transaksi" class="w_normal">Dari</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="dari_transaksi" name="dari_transaksi" readonly="readonly" value="">
                      </div>
                      <div class="form-group">
                        <label for="jenis_transaksi" class="w_normal">Jenis Transaksi</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="jenis_transaksi" name="jenis_transaksi" readonly="readonly" value="">
                      </div>
                      <div class="form-group">
                        <label for="jumlah_transaksi" class="w_normal">Jumlah Total</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="jumlah_transaksi" name="jumlah_transaksi" readonly="readonly" value="">
                      </div>
                      <div class="form-group">
                        <label for="tanggal_transaksi" class="w_normal">Tanggal Pembayaran</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="tanggal_transaksi" name="tanggal_transaksi" readonly="readonly" value="">
                      </div>
                      <div class="form-group">
                        <label for="record_transaksi" class="w_normal">Jumlah Record</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="record_transaksi" name="record_transaksi" readonly="readonly" value="">
                      </div>
                      <div class="form-group">
                        <label for="file_transaksi" class="w_normal">Imported File</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="file_transaksi" name="file_transaksi" readonly="readonly" value="">
                      </div>
                      <div class="form-group">
                        <label for="status_transaksi" class="w_normal">Status</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="status_transaksi" name="status_transaksi" readonly="readonly" value="">
                      </div>
                      <div class="form-group">
                        <label for="keterangan_transaksi" class="w_normal">Keterangan</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="keterangan_transaksi" name="keterangan_transaksi" readonly="readonly" value="">
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 mb_40">
                        <button type="button" id="button_cancel" class="btn btn-default button_white fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Kembali</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <?php include( PLUGPATH ."_footer.php"); ?>
        
      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
</html>
<?php ob_flush(); ?>