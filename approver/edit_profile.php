<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "managemember";
    $subpage = "edit";
    $page_name = "Edit Profile";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        
        <?php include('_nav.php'); ?>

        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Rincian Transaksi
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mb_40">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
                <form>
                  <div class="row clearfix">
                    <div class="col-xs-12 mt_40">
                      <div class="form-group">
                        <label for="name" class="w_normal">Name</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="name" name="name" value="">
                      </div>
                      <div class="form-group">
                        <label for="email" class="w_normal">Email</label>
                        <input type="email" class="form-control br_0 bg_white c_grey" id="email" name="email" value="">
                      </div>
                      <div class="form-group">
                        <label for="old_password" class="w_normal">Password Lama</label>
                        <input type="password" class="form-control br_0 bg_white c_grey" id="old_password" name="old_password" value="">
                      </div>
                      <div class="form-group">
                        <label for="new_password" class="w_normal">Password Baru</label>
                        <input type="password" class="form-control br_0 bg_white c_grey" id="new_password" name="new_password" value="">
                      </div>
                      <div class="form-group">
                        <label for="phone" class="w_normal">Phone</label>
                        <input type="number" class="form-control br_0 bg_white c_grey" id="phone" name="phone" value="">
                      </div>
                    </div>
                    <div class="col-xs-12 mb_40">
                      <div class="right">
                        <button type="button" id="button_cancel" class="btn btn-default button_white fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Batalkan</button>
                        <button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Simpan</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        
        <?php include( PLUGPATH ."_footer.php"); ?>
        
      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
</html>
<?php ob_flush(); ?>