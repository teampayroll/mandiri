<?php
    if(!isset($_SESSION)){session_start();}
	ob_start();

	include("../_fileinclude.php");
	
	// get POST data
	$idschedule = trim($_POST["idschedule"]);
	
	// delete this if session header activated -- for TESTING ONLY
	if (!isset($adminId)) {$adminId="3";}
	if (!isset($adminName)) {$adminName="Administrator";}
	if (!isset($adminLogin)) {$adminLogin="3rikpas@gmail.com";}
	
	// generate OTP code
	$otp_code = randomKey(4);
	
	// get team type for update OTP send (as user login) via ajax
	$teamtype = "0";
	$query = "  SELECT team
				FROM mst_user
				WHERE status <> 2 AND id = '$adminId'";
	$result = mysqlQuery($query);
	if(mysqlNumRows($result)){
		$row = mysqlFetchArray($result);
		$teamtype = trim($row['team']);
	}mysqlFreeResult($result);
	
	// check approver team
	$add_query = "";
	if ($teamtype=="1") {$add_query = " idapprover_a = $adminId, otp_code_a = '$otp_code' ";}
	if ($teamtype=="2") {$add_query = " idapprover_b = $adminId, otp_code_b = '$otp_code' ";}
	
	// save approver & OTP code
	if ($add_query!="") {
		$query = "UPDATE mst_schedule 
				  SET updatedby = '$adminId', updateddate = '$datepost', ipaddress = '$ipaddresspost',  
				  $add_query 
				  WHERE id = '$idschedule'";
		mysqlQuery($query);
	}	
	
	// send email OTP
	$to = array($adminLogin => $adminName);
	$from = array('otp@ecashmandiri.com' => 'OTP Generate Code'); 
	$subject = "OTP Generate Code (e-Cash Mandiri)";
	
	// generate email body
	$email_body = "Anda telah melakukan permintaan untuk pengiriman kode otorisasi. <br /><br />";
	$email_body .= "Kode otorisasi Anda adalah : $otp_code <br /><br />";
	$email_body .= "Silahkan masukkan kode otorisasi diatas untuk melanjutkan proses otorisasi. <br /><br /><br />";
	$email_body .= "Anda menerima email ini karena Anda baru saja melakukan permintaan kode otorisasi. Jangan dibalas email ini, karena semua email yang masuk tidak akan diproses.";
	
	// load email html template
	$message = ''; 
	$fh = fopen('emailotp_template.html', 'r'); // read	file	
	$message = fread($fh, filesize('emailotp_template.html'));
	$message = str_replace('[%full_url%]', siteUrl(), $message);
	$message = str_replace('[%username%]', $adminName, $message);
	$message = str_replace('[%content%]', $email_body, $message);
	fclose($fh);
		
	// send email OTP	
	email($to, $from, $subject, $message);
		
	ob_flush();
?>