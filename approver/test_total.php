<?php
    if(!isset($_SESSION)){session_start();}
	ob_start();

	include("../_fileinclude.php");
	
	// get CSV file
	$csvFile = file('../lib/412schedule2.csv', FILE_IGNORE_NEW_LINES);	
	$data = array();
	$total = 0;
	
	// get data from CSV line
	foreach ($csvFile as $key => $value) {
		$data[$key] = str_getcsv($value);		
	}
	
	// only count total row (array_number: 2)
	foreach ($data as $keys => $values) {
		foreach ($values as $key => $value) {
			if ($key=='2') {$total = ($total + $value);}
		}
	}	
				
	echo "Total: ".$total;
	
	ob_flush();
?>