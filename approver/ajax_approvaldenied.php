<?php
    if(!isset($_SESSION)){session_start();}
	ob_start();

	include("../_fileinclude.php");
	
	function chk_invaliddate($paydate) {
		$valid_time = date('Y-m-d 23:00:00',strtotime($paydate)); // 23:00
		if (date('Y-m-d H:i:s') > $valid_time) { // invalid time entry
			return true;
		} else {
			return false;
		}
	}
	
	function chk_invalidtotal($totalpay,$csvfile) {
		// get CSV file
		$csvFile = file('../lib/'.$csvfile, FILE_IGNORE_NEW_LINES);
		$data = array();
		$total_csv = 0;
		
		// get data from CSV line
		foreach ($csvFile as $key => $value) {
			$data[$key] = str_getcsv($value);		
		}
		
		// only count total row (array_number: 2)
		foreach ($data as $keys => $values) {
			foreach ($values as $key => $value) {
				if ($key=='2') {$total_csv = ($total_csv + $value);}
			}
		}	
		
		if ($total_csv==$totalpay) {return false;} else {return true;}
	}
	
	// get POST data
	$type    = trim($_POST["type"]);
	$idschedule = trim($_POST["idschedule"]);
	$paydate = trim($_POST["paydate"]);
	$totalpay = trim($_POST["totalpay"]);
	$csvfile = trim($_POST["csvfile"]);
	$otp     = trim($_POST["otp"]);
	$comment = trim($_POST["comment"]);
	
	// delete this if session header activated -- for TESTING ONLY
	if (!isset($adminId)) {$adminId="3";}
	
	// get team type for update schedule (as user login) via ajax
	$teamtype = "0";
	$query = "  SELECT team
				FROM mst_user
				WHERE status <> 2 AND id = '$adminId'";
	$result = mysqlQuery($query);
	if(mysqlNumRows($result)){
		$row = mysqlFetchArray($result);
		$teamtype = trim($row['team']);
	}mysqlFreeResult($result);
	
	// check extended validation	
	$time_chk = "";
	$total_chk = "";
	if ($type=="1") { // active only in approval system
		// check aprover valid time (maks. 11 PM)	
		if (chk_invaliddate($paydate)) {
			$type = "2"; // auto denied system
			$time_chk = "time-invalid";
		}
		
		// check aprover total with CSV
		if (chk_invalidtotal($totalpay,$csvfile)) {
			$total_chk = "total-invalid";
		}
	}
	
	// check approver team
	$add_query = "";
	$add_otp = "";
	if ($teamtype=="1") {$add_query = " commentapprover_a = '$comment', status_approver_a = $type "; $add_otp = "otp_code_a";}
	if ($teamtype=="2") {$add_query = " commentapprover_b = '$comment', status_approver_b = $type "; $add_otp = "otp_code_b";}
	
	// get OTP aprover for validation
	$OTPdata = "";
	if ($add_otp!="") {
		$query = "  SELECT $add_otp
					FROM mst_schedule
					WHERE status <> 2 AND id = '$idschedule'";
		$result = mysqlQuery($query);
		if(mysqlNumRows($result)){
			$row = mysqlFetchArray($result);
			$OTPdata = trim($row[$add_otp]);
		}mysqlFreeResult($result);
	}
	
	// check OTP validation
	$result_chk = "otp-invalid";
	if ($OTPdata!="") {
		if ($OTPdata==$otp) {$result_chk = "otp-valid";} // valid OTP entry
	}
	
	// only valid OTP entry can continue
	if ($result_chk=="otp-valid") {
		if ($type=="1" || $type=="2") { // approval & denial process			
			// save approver data
			if ($add_query!="" && $total_chk=="") {
				// update per approver request
				$query = "UPDATE mst_schedule 
						  SET updatedby = '$adminId', updateddate = '$datepost', ipaddress = '$ipaddresspost',  
						  $add_query 
						  WHERE id = '$idschedule'";
				mysqlQuery($query);
				
				// auto update schedule status as mst_setting (3:approved|4:denied)
				$def_type = "1";
				$query = "  SELECT type
							FROM mst_setting ";
				$result = mysqlQuery($query);
				if(mysqlNumRows($result)){
					$row = mysqlFetchArray($result);
					$def_type = trim($row["type"]);
				}mysqlFreeResult($result);
				
				// get approver status
				$status_approver_a = "0";
				$status_approver_b = "0";
				$query = "  SELECT status_approver_a,status_approver_b
							FROM mst_schedule
							WHERE status <> 2 AND id = '$idschedule'";
				$result = mysqlQuery($query);
				if(mysqlNumRows($result)){
					$row = mysqlFetchArray($result);
					$status_approver_a = trim($row[$status_approver_a]);
					$status_approver_b = trim($row[$status_approver_b]);
				}mysqlFreeResult($result);
				
				// one approver check - logic approval
				$query_update = "";
				if ($def_type=="1") {					
					if ($status_approver_a=="1" || $status_approver_b=="1") {
						$query_update = " status = 3 "; // approved
					} else {
						if ($status_approver_a=="2" || $status_approver_b=="2") {
							$query_update = " status = 4 "; // denied
						} 
					}					
				}
				// two approver check - logic approval
				if ($def_type=="2") {					
					if ($status_approver_a=="1" && $status_approver_b=="1") {$query_update = " status = 3 ";} // approved
					if (($status_approver_a=="1" && $status_approver_b=="2") || ($status_approver_a=="2" && $status_approver_b=="1") || ($status_approver_a=="2" && $status_approver_b=="2")) {$query_update = " status = 4 ";} // denied
				}
				
				// update main schedule status
				if ($query_update!="") {						
					$query = "UPDATE mst_schedule 
							  SET $query_update 
							  WHERE id = '$idschedule'";
					mysqlQuery($query);
				}
			}			
		}
		
		if ($time_chk=="time-invalid") {$result_chk = $time_chk;} // valid OTP but not time entry
		if ($total_chk=="total-invalid") {$result_chk = $total_chk;} // valid OTP but not total entry
	}	
	
	echo $result_chk;
	
	ob_flush();
?>