-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2016 at 04:20 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecash`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_history_activity`
--

CREATE TABLE `mst_history_activity` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: login | 1: logout | 2: one to many / schedule | 3: bulk member | 4: approval one to many | 5: approval bulk member',
  `idrelated` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:active | 1:nonactive | 2:deleted',
  `createdby` int(11) NOT NULL DEFAULT '0',
  `createddate` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `updatedby` int(11) NOT NULL DEFAULT '0',
  `updateddate` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `ipaddress` varchar(20) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_history_activity`
--

INSERT INTO `mst_history_activity` (`id`, `iduser`, `type`, `idrelated`, `status`, `createdby`, `createddate`, `updatedby`, `updateddate`, `ipaddress`) VALUES
(1, 6, 3, 3, 0, 6, '1900-01-01 00:00:00', 0, '2016-11-23 14:57:44', ' '),
(2, 6, 0, 0, 0, 6, '1900-01-01 00:00:00', 0, '2016-11-23 14:57:44', ' '),
(3, 6, 2, 1, 0, 6, '1900-01-01 00:00:00', 0, '2016-11-23 14:57:44', ' '),
(4, 6, 1, 0, 0, 6, '1900-01-01 00:00:00', 0, '2016-11-23 14:57:44', ' '),
(5, 3, 5, 3, 0, 3, '1900-01-01 00:00:00', 0, '1900-01-01 00:00:00', ' '),
(6, 4, 4, 1, 0, 4, '1900-01-01 00:00:00', 0, '1900-01-01 00:00:00', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `mst_notification`
--

CREATE TABLE `mst_notification` (
  `id` int(11) NOT NULL,
  `type_notif` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: submitter to approver | 1: approver to approver',
  `type_submitter` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: upload schedule | 1: h-2 schedule | 2: h-1 schedule',
  `type_approver` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: team a | 1: team b | 2: all',
  `idrelated` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:active | 1:nonactive | 2:deleted',
  `createdby` int(11) NOT NULL DEFAULT '0',
  `createddate` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `updatedby` int(11) NOT NULL DEFAULT '0',
  `updateddate` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `ipaddress` varchar(20) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_schedule`
--

CREATE TABLE `mst_schedule` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL DEFAULT '0',
  `noreftransaction` int(11) NOT NULL DEFAULT '0',
  `total` double(15,0) NOT NULL DEFAULT '0',
  `jenistransaction` varchar(255) NOT NULL DEFAULT ' ',
  `totalrecord` int(11) NOT NULL DEFAULT '0',
  `tanggalpembayaran` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `filecsv` varchar(255) NOT NULL DEFAULT ' ',
  `infosubmitter` text NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: original file | 1: encrypted file',
  `idapprover_a` int(11) NOT NULL DEFAULT '0',
  `otp_code_a` varchar(4) NOT NULL DEFAULT ' ',
  `commentapprover_a` text NOT NULL,
  `status_approver_a` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: default | 1: approved | 2: denied',
  `idapprover_b` int(11) NOT NULL DEFAULT '0',
  `otp_code_b` varchar(4) NOT NULL DEFAULT ' ',
  `commentapprover_b` text NOT NULL,
  `status_approver_b` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: default | 1: approved | 2: denied',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:active | 1:nonactive | 2:deleted',
  `createdby` int(11) NOT NULL DEFAULT '0',
  `createddate` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `updatedby` int(11) NOT NULL DEFAULT '0',
  `updateddate` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `ipaddress` varchar(20) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_schedule`
--

INSERT INTO `mst_schedule` (`id`, `iduser`, `noreftransaction`, `total`, `jenistransaction`, `totalrecord`, `tanggalpembayaran`, `filecsv`, `infosubmitter`, `type`, `idapprover_a`, `otp_code_a`, `commentapprover_a`, `status_approver_a`, `idapprover_b`, `otp_code_b`, `commentapprover_b`, `status_approver_b`, `status`, `createdby`, `createddate`, `updatedby`, `updateddate`, `ipaddress`) VALUES
(1, 6, 2147483647, 100000, 'Transaksi 1', 20, '2016-11-26 00:00:00', '897264803schedule2.csv', 'Keterangan', 0, 0, ' ', 'should not showed up', 0, 4, ' ', 'Comment approver b', 0, 1, 6, '2016-11-24 08:34:03', 6, '2016-11-24 08:34:03', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_setting`
--

CREATE TABLE `mst_setting` (
  `id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: 1 Approver | 1: 2 Approver'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_setting`
--

INSERT INTO `mst_setting` (`id`, `type`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_update_member`
--

CREATE TABLE `mst_update_member` (
  `id` int(11) NOT NULL,
  `iduser` int(11) NOT NULL DEFAULT '0',
  `filecsv` varchar(255) NOT NULL DEFAULT ' ',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: original file | 1: encrypted file',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:active | 1:nonactive | 2:deleted',
  `infosubmitter` text NOT NULL,
  `idapprover_a` int(11) NOT NULL,
  `otp_code_a` varchar(4) NOT NULL,
  `commentapprover_a` text NOT NULL,
  `status_approver_a` int(2) NOT NULL,
  `idapprover_b` int(11) NOT NULL,
  `otp_code_b` varchar(4) NOT NULL,
  `commentapprover_b` text NOT NULL,
  `status_approver_b` int(2) NOT NULL,
  `createdby` int(11) NOT NULL DEFAULT '0',
  `createddate` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `updatedby` int(11) NOT NULL DEFAULT '0',
  `updateddate` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `ipaddress` varchar(20) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_update_member`
--

INSERT INTO `mst_update_member` (`id`, `iduser`, `filecsv`, `type`, `status`, `infosubmitter`, `idapprover_a`, `otp_code_a`, `commentapprover_a`, `status_approver_a`, `idapprover_b`, `otp_code_b`, `commentapprover_b`, `status_approver_b`, `createdby`, `createddate`, `updatedby`, `updateddate`, `ipaddress`) VALUES
(3, 6, '443517867PKH.csv', 0, 0, 'Update 1', 3, '', 'comment approver a', 0, 0, '', '', 0, 6, '2016-11-24 08:32:20', 6, '2016-11-24 08:32:20', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_user`
--

CREATE TABLE `mst_user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT ' ',
  `email` varchar(255) NOT NULL DEFAULT ' ',
  `password` varchar(50) NOT NULL DEFAULT ' ',
  `phone` varchar(20) NOT NULL DEFAULT ' ',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Submitter | 1: Approver | 2: Superadmin',
  `team` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Default | 1: Approver A | 2: Approver B',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:active | 1:nonactive | 2:deleted',
  `createdby` int(11) NOT NULL DEFAULT '0',
  `createddate` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `updatedby` int(11) NOT NULL DEFAULT '0',
  `updateddate` datetime NOT NULL DEFAULT '1900-01-01 00:00:00',
  `ipaddress` varchar(20) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_user`
--

INSERT INTO `mst_user` (`id`, `name`, `email`, `password`, `phone`, `type`, `team`, `status`, `createdby`, `createddate`, `updatedby`, `updateddate`, `ipaddress`) VALUES
(1, 'vincent', 'vincentputragunawan@yahoo.com', 'Mf2GqzX2l1KDFme7BbED09e6WpSX', '085755789222', 2, 0, 0, 0, '2016-11-08 00:00:00', 0, '2016-11-08 00:00:00', '::1'),
(2, 'submitter', 'submitter@gmail.com', 'bgM08ZLYIlg/qDGAFIAIZfavUR9P', '085755789222', 0, 0, 0, 0, '2016-11-08 00:00:00', 2, '2016-11-09 15:04:34', '::1'),
(3, 'approver a', 'approvera@gmail.com', 'ZAJAedfXIlhiECO0pjcD4FxXJTTx', '085755781122', 1, 1, 0, 0, '2016-11-08 00:00:00', 3, '2016-11-09 15:01:27', '::1'),
(4, 'approver b', 'approverb@gmail.com', 'Mf2GqzX2l1KDFme7BbED09e6WpSX', '085755789222', 1, 2, 1, 0, '2016-11-08 00:00:00', 0, '2016-11-08 00:00:00', '::1'),
(5, 'kevin', 'kevin.hutama91@gmail.com', '8f9InMXHM1h66+J5Jxw=', '08970647285', 2, 0, 0, 1, '2016-11-22 11:21:25', 1, '2016-11-22 11:21:25', ' '),
(6, 'Submitter 2', 'submitter2@gmail.com', 'sv5WOc7IM1jx1LARCN0=', '0898844742542', 0, 0, 0, 5, '2016-11-22 11:25:50', 6, '2016-11-23 15:54:48', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `tr_notification`
--

CREATE TABLE `tr_notification` (
  `iduser` int(11) NOT NULL DEFAULT '0',
  `idnotif` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_useraccess`
--

CREATE TABLE `tr_useraccess` (
  `iduser` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT ' ',
  `idaccess` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_useraccess`
--

INSERT INTO `tr_useraccess` (`iduser`, `name`, `idaccess`) VALUES
(5, 'manage user', 7),
(5, 'manage access', 8),
(5, 'manage setting', 9),
(5, 'edit profile', 5),
(6, 'bulk update', 1),
(6, 'schedule', 2),
(6, 'history activity', 3),
(6, 'history otorisasi', 4),
(6, 'edit profile', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_history_activity`
--
ALTER TABLE `mst_history_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_notification`
--
ALTER TABLE `mst_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_schedule`
--
ALTER TABLE `mst_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_setting`
--
ALTER TABLE `mst_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_update_member`
--
ALTER TABLE `mst_update_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_user`
--
ALTER TABLE `mst_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_history_activity`
--
ALTER TABLE `mst_history_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mst_notification`
--
ALTER TABLE `mst_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_schedule`
--
ALTER TABLE `mst_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_setting`
--
ALTER TABLE `mst_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mst_update_member`
--
ALTER TABLE `mst_update_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mst_user`
--
ALTER TABLE `mst_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
