<?php
	function mysqlConnect( $host, $user, $pass, $flag = false, $phpversion = '' ) // Opens a new connection to the MySQL server
	{
		if($phpversion==""){
			$phpversion = phpversion();
		}

		if( $phpversion >= '5.5.0' ){
			$mysqlConnect = mysqli_connect($host, $user, $pass);
		}else{
			$mysqlConnect = mysql_connect($host, $user, $pass, $flag);
		}

		return $mysqlConnect;
	}

	function mysqlDB( $db, $conn, $phpversion = '' ) // Changes the default database for the connection
	{
		if($phpversion==""){
			$phpversion = phpversion();
		}

		if( $phpversion >= '5.5.0' ){
			$mysqlDB = mysqli_select_db($conn, $db);
		}else{
			$mysqlDB = mysql_select_db($db, $conn);
		}

		return $mysqlDB;
	}

	function mysqlClose( $conn, $phpversion = '' ) // Changes the default database for the connection
	{
		if($phpversion==""){
			$phpversion = phpversion();
		}

		if( $phpversion >= '5.5.0' ){
			$mysqlClose = mysqli_close($conn);
		}else{
			$mysqlClose = mysql_close($conn);
		}

		return $mysqlClose;
	}

	function mysqlQuery( $query, $phpversion = '' ) // Performs a query against the database
	{
		if($phpversion==""){
			$phpversion = phpversion();
		}

		global $conn;

		if( $phpversion >= '5.5.0' ){
			$mysqlQuery = mysqli_query($conn, $query);
		}else{
			$mysqlQuery = mysql_query($query);
		}

		return $mysqlQuery;
	}

	function mysqlNumRows( $result, $phpversion = '' ) // Returns the number of rows in a result set
	{
		if($phpversion==""){
			$phpversion = phpversion();
		}

		if( $phpversion >= '5.5.0' ){
			$mysqlNumRows = mysqli_num_rows($result);
		}else{
			$mysqlNumRows = mysql_num_rows($result);
		}

		return $mysqlNumRows;
	}

	function mysqlFetchArray( $result, $phpversion = '' ) // Fetches a result row as an associative, a numeric array, or both
	{
		if($phpversion==""){
			$phpversion = phpversion();
		}

		if( $phpversion >= '5.5.0' ){
			$mysqlFetchArray = mysqli_fetch_array($result);
		}else{
			$mysqlFetchArray = mysql_fetch_array($result);
		}

		return $mysqlFetchArray;
	}

	function mysqlFreeResult( $result, $phpversion = '' ) // Frees the memory associated with a result
	{
		if($phpversion==""){
			$phpversion = phpversion();
		}

		if( $phpversion >= '5.5.0' ){
			$mysqlFreeResult = mysqli_free_result($result);
		}else{
			$mysqlFreeResult = mysql_free_result($result);
		}

		return $mysqlFreeResult;
	}
?>