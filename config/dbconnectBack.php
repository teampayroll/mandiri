<?php
    /*
     * Connects to the database using the parameters from above.
     * BE SURE TO CHANGE THE DATABASE PARAMS TO REFLECT YOUR DATABASE SETTINGS!
    */

    // get setting database
    $access = accessBackend();

    $connBack = mysqlConnect($access['0'], $access['1'], $access['2'], true, $access['4']);
    $dbBack = mysqlDB($access['3'], $connBack);
    if(!$connBack) { // if host fail
        header("Location: $Config_site/maintenance");
    }else{
        if(!$dbBack) { // if db connection fail 
            header("Location: $Config_site/maintenance");
        }
    }

    if(get_magic_quotes_gpc()){
        function stripslashes_deep($value)
        {
            $value = is_array($value) ?
                        array_map('stripslashes_deep', $value) :
                        stripslashes($value);

            return $value;
        }

        $_POST = array_map('stripslashes_deep', $_POST);
        $_GET = array_map('stripslashes_deep', $_GET);
        $_COOKIE = array_map('stripslashes_deep', $_COOKIE);
        $_REQUEST = array_map('stripslashes_deep', $_REQUEST);
    }
?>
