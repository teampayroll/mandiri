<?php
	function accessBackend() // the function for get access to database backend
	{
		global $Config_backend;

		$urltopost = $Config_backend."access.php";
		$data = "";

		$ch = curl_init ($urltopost);
		curl_setopt($ch, CURLOPT_URL, "$urltopost");
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS,"$data");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$returndata = curl_exec ($ch);

		$access = explode(";", $returndata);

		return $access;
	}

	function termBackend( $action, $data = '' ) // the function for get access to database backend
	{
		global $Config_backend;

		$urltopost = $Config_backend."mst_setting.php?action=".$action;

		$ch = curl_init ($urltopost);
		curl_setopt($ch, CURLOPT_URL, "$urltopost");
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS,"$data");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$returndata = curl_exec ($ch);
		$array = json_decode($returndata,true);

		return $array;
	}

	function informationBackend( $action, $data = '' ) // the function for get access to database backend
	{
		global $Config_backend;

		$urltopost = $Config_backend."tr_information.php?action=".$action;

		$ch = curl_init ($urltopost);
		curl_setopt($ch, CURLOPT_URL, "$urltopost");
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS,"$data");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$returndata = curl_exec ($ch);
		$array = json_decode($returndata,true);

		return $array;
	}

	function clientBackend( $action, $data = '' ) // the function for get access to database backend
	{
		global $Config_backend;

		$urltopost = $Config_backend."mst_client.php?action=".$action;

		$ch = curl_init ($urltopost);
		curl_setopt($ch, CURLOPT_URL, "$urltopost");
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS,"$data");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$returndata = curl_exec ($ch);
		$array = json_decode($returndata,true);

		return $array;
	}

	function supportBackend( $action, $data = '' ) // the function for get access to database backend
	{
		global $Config_backend;

		$urltopost = $Config_backend."mst_support.php?action=".$action;

		$ch = curl_init ($urltopost);
		curl_setopt($ch, CURLOPT_URL, "$urltopost");
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS,"$data");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$returndata = curl_exec ($ch);
		$array = json_decode($returndata,true);

		return $array;
	}

	function departmentBackend( $action, $data = '' ) // the function for get access to database backend
	{
		global $Config_backend;

		$urltopost = $Config_backend."mst_department.php?action=".$action;

		$ch = curl_init ($urltopost);
		curl_setopt($ch, CURLOPT_URL, "$urltopost");
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS,"$data");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$returndata = curl_exec ($ch);
		$array = json_decode($returndata,true);

		return $array;
	}

	function adminBackend( $action, $data = '' ) // the function for get access to database backend
	{
		global $Config_backend;

		$urltopost = $Config_backend."mst_admin.php?action=".$action;

		$ch = curl_init ($urltopost);
		curl_setopt($ch, CURLOPT_URL, "$urltopost");
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS,"$data");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$returndata = curl_exec ($ch);
		$array = json_decode($returndata,true);

		return $array;
	}

	function chatBackend( $action, $data = '' ) // the function for get access to database backend
	{
		global $Config_backend;

		$urltopost = $Config_backend."mst_chat.php?action=".$action;

		$ch = curl_init ($urltopost);
		curl_setopt($ch, CURLOPT_URL, "$urltopost");
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS,"$data");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$returndata = curl_exec ($ch);
		$array = json_decode($returndata,true);

		return $array;
	}

	function newsBackend( $action, $data = '' ) // the function for get access to database backend
	{
		global $Config_backend;

		$urltopost = $Config_backend."mst_news.php?action=".$action;

		$ch = curl_init ($urltopost);
		curl_setopt($ch, CURLOPT_URL, "$urltopost");
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS,"$data");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$returndata = curl_exec ($ch);
		$array = json_decode($returndata,true);

		return $array;
	}

	function helpBackend( $action, $data = '' ) // the function for get access to database backend
	{
		global $Config_backend;

		$urltopost = $Config_backend."mst_help.php?action=".$action;

		$ch = curl_init ($urltopost);
		curl_setopt($ch, CURLOPT_URL, "$urltopost");
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS,"$data");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$returndata = curl_exec ($ch);
		$array = json_decode($returndata,true);

		return $array;
	}

	function notifBackend( $action, $data = '' ) // the function for get access to database backend
	{
		global $Config_backend;

		$urltopost = $Config_backend."tr_notif.php?action=".$action;

		$ch = curl_init ($urltopost);
		curl_setopt($ch, CURLOPT_URL, "$urltopost");
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS,"$data");
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		$returndata = curl_exec ($ch);
		$array = json_decode($returndata,true);

		return $array;
	}
?>