<?php
	function secureSql( $param ) // prevent SQL injection attack
	{		
		$param = htmlentities( $param, ENT_QUOTES );

		if( get_magic_quotes_gpc() ){ // prevents duplicate backslashes
			$param = stripslashes( $param );
		}

		/*if( phpversion() >= '4.3.0' ){ // if using new version of PHP and mysql_real_escape_string
			$param = mysql_real_escape_string( $param );
		}else{ // for the old version of PHP and mysql_escape_string
			$param = mysql_escape_string( $param );
		}*/
		
		if( ctype_digit( $param ) ){ // get the integer value
			$param = intval( $param ); 
		}

		return $param;
	}

	function cleanInput( $param ) // clean input from xss (cross site scripting)
	{
		$filter = array(
		'@<script[^>]*?>.*?</script>@si',   // strip out javascript
	    '@<[\/\!]*?[^<>]*?>@si',            // strip out HTML tags
	    '@<style[^>]*?>.*?</style>@siU',    // strip style tags properly
	    '@<![\s\S]*?--[ \t\n\r]*>@'         // strip multi-line comments
		);

		$param = preg_replace( $filter, '', $param);

		// replace sql injection input
		/*$param = preg_replace( '/\bor\b/i' , '', $param);
		$param = preg_replace( '/\bOR\b/i' , '', $param);*/
		/*$param = preg_replace( '/\band\b/i' , '', $param);
		$param = preg_replace( '/\bAND\b/i' , '', $param);*/
		/*$param = preg_replace( '/\bfrom\b/i' , '', $param);
		$param = preg_replace( '/\bFROM\b/i' , '', $param);*/
		/*$param = preg_replace( '/\binto\b/i' , '', $param);
		$param = preg_replace( '/\bINTO\b/i' , '', $param);*/
		$param = preg_replace( '/\binsert\b/i' , '', $param);
		$param = preg_replace( '/\bINSERT\b/i' , '', $param);
		$param = preg_replace( '/\bupdate\b/i' , '', $param);
		$param = preg_replace( '/\bUPDATE\b/i' , '', $param);
		$param = preg_replace( '/\bset\b/i' , '', $param);
		$param = preg_replace( '/\bSET\b/i' , '', $param);
		/*$param = preg_replace( '/\bwhere\b/i' , '', $param);
		$param = preg_replace( '/\bWHERE\b/i' , '', $param);*/
		$param = preg_replace( '/\bdrop\b/i' , '', $param);
		$param = preg_replace( '/\bDROP\b/i' , '', $param);
		$param = preg_replace( '/\bvalues\b/i' , '', $param);
		$param = preg_replace( '/\bVALUES\b/i' , '', $param);
		$param = preg_replace( '/\bnull\b/i' , '', $param);
		$param = preg_replace( '/\bNULL\b/i' , '', $param);
		$param = preg_replace( '/\bNULL\b/i' , '', $param);
		$param = preg_replace( '/\bdeclare\b/i' , '', $param);
		$param = preg_replace( '/\bDECLARE\b/i' , '', $param);
		$param = preg_replace( '/\bscript\b/i' , '', $param);
		$param = preg_replace( '/\bSCRIPT\b/i' , '', $param);
		$param = preg_replace( '/\bselect\b/i' , '', $param);
		$param = preg_replace( '/\bSELECT\b/i' , '', $param);
		$param = preg_replace( '/\bdelete\b/i' , '', $param);
		$param = preg_replace( '/\bDELETE\b/i' , '', $param);
		/*$param = preg_replace( '/\bcreate\b/i' , '', $param);
		$param = preg_replace( '/\bCREATE\b/i' , '', $param);*/
		$param = preg_replace( '/\bconvert\b/i' , '', $param);
		$param = preg_replace( '/\bCONVERT\b/i' , '', $param);
		$param = preg_replace( '/\bunion\b/i' , '', $param);
		$param = preg_replace( '/\bUNION\b/i' , '', $param);
		$param = preg_replace( '/\b%20union%20\b/i', '', $param);
		$param = preg_replace( '/\b\*union\*\b/i', '', $param);
		$param = preg_replace( '/\b\+union\+\b/i', '', $param);
		$param = preg_replace( '/\b\*\b/i', '', $param);
		$param = preg_replace( '/\border\b/i', '', $param);
		$param = preg_replace( '/\bORDER\b/i', '', $param);
		/*$param = preg_replace( '/\bby\b/i', '', $param);
		$param = preg_replace( '/\bBY\b/i', '', $param);*/
		/*$param = preg_replace( '/\bjoin\b/i', '', $param);
		$param = preg_replace( '/\bJOIN\b/i', '', $param);*/
		$param = preg_replace( '/\blimit\b/i', '', $param);
		$param = preg_replace( '/\bLIMIT\b/i', '', $param);
		$param = preg_replace( '/\bdesc\b/i', '', $param);
		$param = preg_replace( '/\bDESC\b/i', '', $param);
		$param = preg_replace( '/\basc\b/i', '', $param);
		$param = preg_replace( '/\bASC\b/i', '', $param);
		/*$param = preg_replace( '/\bon\b/i', '', $param);
		$param = preg_replace( '/\bON\b/i', '', $param);*/

		return $param;
	}

	function limitStr( $param, $length = 255 ) // limit string input
	{
		$param = substr( $param, 0, $length ); // limit length of string
		
		return $param;
	}
	
	function secureLimit( $param, $length = 255) // get secure field with limit length
	{
		if ( is_array( $param ) ) { // check if variable is array
	        foreach( $param as $var => $val ) {
	            $param[$var] = secureLimit( $val );
	        }
	    }
	    else {
	        $param  = secureSql( strip_tags( str_replace( "'", "\'", str_replace( "\'", "'", cleanInput( limitStr( trim( $param ), $length ) ) ) ) ) );
	    }

	    return $param;
	}

	function secureField( $param ) // get secure field
	{
		if ( is_array( $param ) ) { // check if variable is array
	        foreach( $param as $var => $val ) {
	            $param[$var] = secureField( $val );
	        }
	    }
	    else {
	        $param  = secureSql( strip_tags( str_replace( "'", "\'", str_replace( "\'", "'", cleanInput( trim( $param ) ) ) ) ) );
	    }

	    return $param;
	}

	function secureText( $param ) // get secure field
	{
	    $param  = str_replace( "\'", "'", trim( $param ) );
	    $param  = str_replace( "'", "\'", trim( $param ) );

	    return $param;
	}

	function secureGet( $param ) // get secure field
	{
	    $param  = str_replace( "'", "", trim( $param ) );
	    $param  = str_replace( "\\", "", trim( $param ) );

	    return $param;
	}


	function secureFile( $param, $name = '', $filter = '' ) // get secure file
	{
		$param = basename( $param );
		$param = str_replace( "'", "", $param );
		$param = str_replace( " ", "-", $param );

		$param = pathinfo( $param );
		$ext = $param['extension'];
		$param = $param['filename'];
		$param = str_replace( ".", "", $param );

		if(!equalStr($name,'')){
			$param = $name;
			$param = str_replace( "'", "", $param );
			$param = str_replace( " ", "-", $param );
			$param = str_replace( ".", "", $param );
		}

		$param = substr( $param, 0, 200 );

		$param = $filter.$param.".".$ext;

		return $param;
	}

	function secureZip( $param )
	{
		$type = $param['type'];
	    $param = pathinfo( $param['name'] );
		$ext = $param['extension'];
	    
	    // Ensures that the correct file was chosen
	    $accepted_types = array('application/zip',
	                            'application/x-zip-compressed',
	                            'multipart/x-zip',
	                            'application/s-compressed');
	 
	    foreach($accepted_types as $mime_type) {
	        if($mime_type == $type) {
	            $param = true;
	            break;
	        }
	    }
	       
	  	//Safari and Chrome don't register zip mime types. Something better could be used here.
	    $param = equalStr( strtolower( $ext ), 'zip' ) ? true: false;

        return $param;
	}

	function secureImage( $param, $uploaddir, $info, $widthImg = '', $heightImg = '', $quality = '100' )
	{
		$image = "";
        if ( equalStr( strtolower( $info['extension'] ), 'jpg' ) || equalStr( strtolower( $info['extension'] ), 'jpeg') ) // continue if this is a JPEG image
        {
        	if( !equalStr($widthImg,'') ){
        		// load image and get image size         
	            $img = imagecreatefromjpeg( "{$uploaddir}{$param}" );
	            $width = imagesx( $img );
	            $height = imagesy( $img );

	            // calculate image size
	            $imgWidth = $widthImg; // default width size
	            $new_width = $imgWidth;
	            if( !equalStr($heightImg,'') ){
	            	$new_height = $heightImg; // default height size
	            }else{
	            	$new_height = floor( $height * ( $imgWidth / $width ) );
	            }

	            // create a new temporary image
	            $tmp_img = imagecreatetruecolor( $new_width, $new_height );

				// copy and resize old image into new image 
	            //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
	            // use imagecopyresampled for create new image be better than use imagecopyresized
	            imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

	            // save thumbnail into a file
	            $image = $param;
	            //imagejpeg( $tmp_img, "{$uploaddir}{$param}" );
	            imagejpeg( $tmp_img, "{$uploaddir}{$param}", $quality);
        	}else{
        		// load image and get image size         
	            $img = imagecreatefromjpeg( "{$uploaddir}{$param}" );
	            $width = imagesx( $img );
	            $height = imagesy( $img );

	            // calculate image size
	            $new_width = $width; // defalt width size
	            $new_height = $height; // default height size


	            // create a new temporary image
	            $tmp_img = imagecreatetruecolor( $new_width, $new_height );

				// copy and resize old image into new image 
	            //imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
	            // use imagecopyresampled for create new image be better than use imagecopyresized
	            imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

	            // save thumbnail into a file
	            $image = $param;
	            //imagejpeg( $tmp_img, "{$uploaddir}{$param}" );
	            imagejpeg( $tmp_img, "{$uploaddir}{$param}", $quality);
        	}
        }
        else if( equalStr( strtolower( $info['extension'] ), 'png' ) ) // continue if this is a PNG image
        {
        	if($quality>90){
        		$quality = 90;
        	}
        	$quality = $quality/10;
        	if( !equalStr($widthImg,'') ){
	            // load image and get image size             
	            $img = imagecreatefrompng( "{$uploaddir}{$param}" );
	            $width = imagesx( $img );
	            $height = imagesy( $img );

	            // calculate image size
	            $imgWidth = $widthImg; // default width size
	            $new_width = $imgWidth;
	            if( !equalStr($heightImg,'') ){
	            	$new_height = $heightImg; // default height size
	            }else{
	            	$new_height = floor( $height * ( $imgWidth / $width ) );
	            }

	            // create a new temporary image
	            $tmp_img = imagecreatetruecolor( $new_width, $new_height );
	            imagealphablending($tmp_img, false);

	            // copy and resize old image into new image 
	            imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
	            imagesavealpha($tmp_img, true);

	            // save thumbnail into a file
	            $image = $param;
	            //imagepng( $tmp_img, "{$uploaddir}{$param}" );
	            imagepng( $tmp_img, "{$uploaddir}{$param}", $quality);
	        }else{
	            // load image and get image size             
	            $img = imagecreatefrompng( "{$uploaddir}{$param}" );
	            $width = imagesx( $img );
	            $height = imagesy( $img );

	            // calculate image size
	            $new_width = $width; // defalt width size
	            $new_height = $height; // default height size
	            

	            // create a new temporary image
	            $tmp_img = imagecreatetruecolor( $new_width, $new_height );
	            imagealphablending($tmp_img, false);

	            // copy and resize old image into new image 
	            imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
	            imagesavealpha($tmp_img, true);

	            // save thumbnail into a file
	            $image = $param;
	            //imagepng( $tmp_img, "{$uploaddir}{$param}" );
	            imagepng( $tmp_img, "{$uploaddir}{$param}", $quality);
	        }
        }

        $param = $image;
		return $param;
	}

	function encryptIt( $param ) // enhanced security with algorithm
	{
		$param = AesCtr::encrypt( $param, "v1nc3nt3ncrypt10n", 256 );
		
		return( $param );
	}
	
	function decryptIt( $param ) // enhanced security with algorithm
	{
		$param = AesCtr::decrypt( $param, "v1nc3nt3ncrypt10n", 256 );
		
		return( $param );
	}

	function equalStr( $param, $equal = '' ) // check these two string have the same value
	{
		if ( trim( $param ) == trim( $equal ) ) {
			return true;
		}

		return false;
	}

	function equalNum( $param, $equal = '0' ) // check these two numeric character have the same value
	{
		if ( trim( $param ) == trim( $equal ) ) {
			return true;
		}

		return false;
	}

	function equalPost( $param, $equal ) // check these two post have the same value
	{
		if ( $_POST[$param] == $_POST[$equal] ) {
			return true;
		}

		return false;
	}

	function equalPostStr( $param, $equal = '' ) // check these post have the same value with the string
	{
		if ( $_POST[$param] == trim( $equal ) ) {
			return true;
		}

		return false;
	}

	function equalPostNum( $param, $equal = '0' ) // check these post have the same value with the numeric character
	{
		if ( $_POST[$param] == trim( $equal ) ) {
			return true;
		}

		return false;
	}

	function equalGet( $param, $equal ) // check these two get have the same value
	{
		if ( $_GET[$param] == $_GET[$equal] ) {
			return true;
		}

		return false;
	}

	function equalGetStr( $param, $equal = '' ) // check these get have the same value with the string
	{
		if ( $_GET[$param] == trim( $equal ) ) {
			return true;
		}

		return false;
	}

	function equalGetNum( $param, $equal = '0' ) // check these get have the same value with the numeric character
	{
		if ( $_GET[$param] == trim( $equal ) ) {
			return true;
		}

		return false;
	}

	function equalSession( $param, $equal ) // check these two session get have the same value
	{
		if ( $_SESSION[$param] == $_SESSION[$equal] ) {
			return true;
		}

		return false;
	}

	function equalSessionStr( $param, $equal = '' ) // check these session have the same value with the string
	{
		if ( $_SESSION[$param] == trim( $equal ) ) {
			return true;
		}

		return false;
	}

	function equalSessionNum( $param, $equal = '0' ) // check these session have the same value with the numeric character
	{
		if ( $_SESSION[$param] == trim( $equal ) ) {
			return true;
		}

		return false;
	}

	function equalCookie( $param, $equal ) // check these two cookie get have the same value
	{
		if ( $_COOKIE[$param] == $_COOKIE[$equal] ) {
			return true;
		}

		return false;
	}

	function equalCookieStr( $param, $equal = '' ) // check these cookie have the same value with the string
	{
		if ( $_COOKIE[$param] == trim( $equal ) ) {
			return true;
		}

		return false;
	}

	function equalCookieNum( $param, $equal = '0' ) // check these cookie have the same value with the numeric character
	{
		if ( $_COOKIE[$param] == trim( $equal ) ) {
			return true;
		}

		return false;
	}

	function postExist( $param ) // checks if a post is exist
	{
		if( ISSET( $_POST[$param] ) ) {
			return true;
		}

		return false;
	}

	function getExist( $param ) // checks if a get is exist
	{
		if( ISSET( $_GET[$param] ) ) {
			return true;
		}

		return false;
	}

	function sessionExist( $param ) // checks if a session is exist
	{
		if( ISSET( $_SESSION[$param] ) && !equalStr( $_SESSION[$param], '' ) ) {
			return true;
		}

		return false;
	}

	function cookieExist( $param ) // checks if a cookie is exist
	{
		if( ISSET( $_COOKIE[$param] ) && !equalStr( $_COOKIE[$param], '' ) ) {
			return true;
		}

		return false;
	}

	function fileExist( $param, $dir = '', $admin = '0' ) // the absolute url file in the site area
	{
		global $Config_foldername;
		global $Config_folderadmin;

		$folderadmin = '';

		if( !equalNum( $admin , '0' ) ){
			$folderadmin = $Config_folderadmin;
		}

		if( !equalStr( $dir , '' ) ){
			$dir = '/'.$dir.'/';
		}else{
			$dir = '/';
		}

		$document = $_SERVER['DOCUMENT_ROOT'];

		if(!equalStr($param,'')){
			if( file_exists( $document.$Config_foldername.$folderadmin.$dir.$param ) ){
				return true;
			}
		}

		return false;
	}

	function siteUrl( $param = '', $admin = '0' ) // the absolute url in the site area
	{
		global $Config_site;
		global $Config_folderadmin;

		$folderadmin = '';

		if( !equalNum( $admin , '0' ) ){
			$folderadmin = $Config_folderadmin;
		}

		$param = $Config_site.$folderadmin."/".$param;

		return $param;
	}

	function redirectSite( $param = '', $admin = '0' ) // redirect to the absolute url in the site area
	{
		global $Config_site;
		global $Config_folderadmin;

		$folderadmin = '';

		if( !equalNum( $admin , '0' ) ){
			$folderadmin = $Config_folderadmin;
		}

		$param = $Config_site.$folderadmin."/".$param;

		header( "Location: $param" );
	}

	function fileSite( $param = '', $dir = '', $admin = '0' ) // the absolute url file in the site area
	{
		global $Config_site;
		global $Config_folderadmin;

		$folderadmin = '';

		if( !equalNum( $admin , '0' ) ){
			$folderadmin = $Config_folderadmin;
		}

		if ( !equalStr( $dir , '' ) ) {
			$param = $Config_site.$folderadmin."/".$dir."/".$param;
		}
		else {
			$param = $Config_site.$folderadmin."/".$param;
		}

		return $param;
	}

	function fileUpload( $param = '', $admin = '0' ) // the absolute url file in the site area
	{
		global $Config_foldername;
		global $Config_folderadmin;

		$folderadmin = '';

		if( !equalNum( $admin , '0' ) ){
			$folderadmin = $Config_folderadmin;
		}

		$document = $_SERVER['DOCUMENT_ROOT'];

		if ( !equalStr( $param , '' ) ) {
			$param = $document.$Config_foldername.$folderadmin."/".$param."/";
		}
		else {
			$param = $document.$Config_foldername.$folderadmin."/";
		}

		return $param;
	}

	function openFile( $param = '', $admin = '0' ) // the absolute url file in the site area
	{
		global $Config_foldername;
		global $Config_folderadmin;

		$folderadmin = '';

		if( !equalNum( $admin , '0' ) ){
			$folderadmin = $Config_folderadmin;
		}

		$document = $_SERVER['DOCUMENT_ROOT'];

		if ( !equalStr( $param , '' ) ) {
			$param = $document.$Config_foldername.$folderadmin."/".$param;
		}
		else {
			$param = $document.$Config_foldername.$folderadmin;
		}

		return $param;
	}

	function openZip( $param, $target = '' ) // the function for open or extracted zip file
	{
	    $zip = new ZipArchive();
	    $x = $zip -> open( $param );
	    if( $x === true ) {
	        $zip -> extractTo( $target );
	        $zip -> close();
	         
	        unlink( $param );

	    	return true;
	    }

	    return false;
	}

	function autoRedirectAdmin( $param, $redirect = '' ) // checks if a user is logged in, if logged in redirects them to page in the admin area
	{
		if( sessionExist( $param ) || cookieExist( $param ) ) {
			redirectSite( $redirect, '1' );
			exit;
		}

		return;
	}

	function autoRedirectLoginAdmin( $param, $redirect = '' ) // checks if a user is logged in, if not redirects them to the login page in the admin area
	{
		if( sessionExist( $param ) ) {
			return;
		}

		if( cookieExist( $param ) ) {
			return;
		}

		redirectSite( $redirect, '1' );
		exit;
	}

	function autoRedirectSite( $param, $redirect = '' ) // checks if a user is logged in, if logged in redirects them to page in the site area
	{
		if( sessionExist( $param ) || cookieExist( $param ) ) {
			redirectSite( $redirect );
			exit;
		}

		return;
	}

	function autoRedirectLoginSite( $param, $redirect = '' ) // checks if a user is logged in, if not redirects them to the login page in the site area
	{
		if( sessionExist( $param ) ) {
			return;
		}

		if( cookieExist( $param ) ) {
			return;
		}

		redirectSite( $redirect );
		exit;
	}

	function userAccess( $param = '' )
	{
		if( sessionExist( $param ) ) {
			return true;
		}

		if( cookieExist( $param ) ) {
			return true;
		}

		return false;
	}
	
	function numFormat( $param, $type = '0' ) // change the number format
	{
		if( equalNum( $type , '0' ) ) { // format: 01
			$num = $param/10;
			if($num<1){
				$param = '0'.$param;
			}
		}
		
		return $param;
	}

	function numDate( $param, $type = '0' ) // change the date format
	{
		$day = date( 'd', strtotime( $param ) );
		$dayname = date( 'D', strtotime( $param ) );
		$month = date( 'm', strtotime( $param ) );
		$monthname = date( 'M', strtotime( $param ) );
		$year = date( 'Y', strtotime( $param ) );
		$hour = date( 'H', strtotime( $param ) );
		$minute = date( 'i', strtotime( $param ) );
		$second = date( 's', strtotime( $param ) );

		if( equalNum( $type , '0' ) ) { // format: dd/mm/YYYY
			$param = $day."/".$month."/".$year;
		}
		
		if( equalNum( $type, '1' ) ) { // format: mm/dd/YYYY
			$param = $month."/".$day."/".$year;
		}

		if( equalNum( $type, '2' ) ) { // format: YYYY/mm/dd
			$param = $year."/".$month."/".$day;
		}

		if( equalNum( $type , '3' ) ) { // format: dd-mm-YYYY
			$param = $day."-".$month."-".$year;
		}

		if( equalNum( $type, '4' ) ) { // format: mm-dd-YYYY
			$param = $month."-".$day."-".$year;
		}

		if( equalNum( $type, '5' ) ) { // format: YYYY-mm-dd
			$param = $year."-".$month."-".$day;
		}
		
		if( equalNum( $type, '6' ) ) { // format: dd-mm-YYYY 00:00:00
			$param = $day."-".$month."-".$year." ".$hour.":".$minute.":".$second;
		}

		if( equalNum( $type, '7' ) ) { // format: mm-dd-YYYY 00:00:00
			$param = $month."-".$day."-".$year." ".$hour.":".$minute.":".$second;
		}

		if( equalNum( $type, '8' ) ) { // format: YYYY-dd-mm 00:00:00
			$param = $year."-".$month."-".$day." ".$hour.":".$minute.":".$second;
		}

		if( equalNum( $type, '9' ) ) { // format: dd/mm/YYYY 00:00:00
			$param = $day."/".$month."/".$year." ".$hour.":".$minute.":".$second;
		}

		if( equalNum( $type, '10' ) ) { // format: mm/dd/YYYY 00:00:00
			$param = $month."/".$day."/".$year." ".$hour.":".$minute.":".$second;
		}

		if( equalNum( $type, '11' ) ) { // format: YYYY/dd/mm 00:00:00
			$param = $year."/".$month."/".$day." ".$hour.":".$minute.":".$second;
		}
		
		if( equalNum( $type, '12' ) ) { // format: 00:00
			$param = $hour.":".$minute.":".$second;
		}

		if( equalNum( $type, '13' ) ) { // format: dd.mm.YYYY
			$param = $day.".".$month.".".$year;
		}

		if( equalNum( $type, '14' ) ) { // format: mm.dd.YYYY
			$param = $month.".".$day.".".$year;
		}

		if( equalNum( $type, '15' ) ) { // format: YYYY.dd.mm
			$param = $year.".".$day.".".$month;
		}

		if( equalNum( $type, '16' ) ) { // format: D, dd MM YYYY - 00.00
			$param = $dayname.", ".$day." ".$monthname." ".$year." - ".$hour.".".$minute;
		}
		
		return $param;
	}

	function strDate( $param, $type = '0' ) // change the date format
	{
		$T['Day'] = array( "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" );
		$T['Day_indo'] = array( "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu" );
		$T['S_Day'] = array( "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" );
		$T['Month'] = array( "", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" );
		$T['Month_indo'] = array( "", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" );
		$T['S_Month'] = array( "", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" );
	   
		$longday = $T['Day'][ date( "w", strtotime( $param ) ) ]; // days full
		$longday_s = $T['S_Day'][ date( "w", strtotime( $param ) ) ]; // days full short
		$day = date( "d", strtotime( $param ) ); // date
		$month = $T['Month'][ date( "n", strtotime( $param ) ) ]; // month full
		$month_s = $T['S_Month'][ date( "n", strtotime( $param ) ) ]; // month full short
		$year = date( "Y", strtotime( $param ) ); // year
		$day_indo = $T['Day_indo'][ date( "w", strtotime( $param ) ) ]; // day full in indo
		$month_indo = $T['Month_indo'][ date( "n", strtotime( $param) ) ]; // month full in indo
		
		$hour = date( "H", strtotime( $param ) ); // Hour
		$minute = date( "i", strtotime( $param ) ); // Minute
	   
		if( equalNum( $type, '0' ) ) { // ex. Monday, January 21, 2001
			$param = $longday.", ".$month." ".$day.", ".$year;
		}
		
		if( equalNum( $type, '1' ) ){ // ex. Mon, Jan 21, 2001
			$param = $longday_s.", ".$month_s." ".$day.", ".$year;
		}
		
		if( equalNum( $type, '2' ) ) { // indo date, ex: 21 Januari 2001
			$param = $day." ".$month_indo." ".$year;
		}
		
		if( equalNum( $type, '3' ) ) { // indo date, ex: Minggu, 21 Januari 2001
			$param = $day_indo.", ".$day." ".$month_indo." ".$year;
		}
		
		if( equalNum( $type, '4' ) ) { // ex: January 21, 2001
			$param = $month." ".$day.", ".$year;
		}
		
		if( equalNum( $type, '5' ) ) { // indo date, ex: 21 Januari 2001 on 14:30
			$param = $day." ".$month_indo." ".$year." on ".$hour.":".$minute;
		}

		if( equalNum( $type, '6' ) ) { // ex: January 2001
			$param = $month." ".$year;
		}
		
		return $param;
	}

	function dateGMT() // indonesia datetime
	{
		// date_default_timezone_set('Asia/Jakarta') = UTC+7;
		$h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
		$hm = $h * 60;
		$ms = $hm * 60;
		$param = gmdate( "Y-m-d,H:i:s", time()+($ms) ); // the "-" can be switched to a plus if that's what your time zone is.
		
		return $param;
	}

	function seoUrl( $param ) // make name be seourl
	{
        $param = strtolower($param); // Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
        
        $param = preg_replace( "/[^a-z0-9_\s-]/", "", $param ); // Strip any unwanted characters
        
        $param = preg_replace( "/[\s-]+/", " ", $param ); // Clean multiple dashes or whitespaces
        
        $param = preg_replace( "/[\s_]/", "-", $param ); //Convert whitespaces and underscore to dash
        
        return $param;
	}

	function randomKey( $length = '10', $type = '0' ) // make random key
	{
		$param = "";
		
		if( equalNum($type,'0') ) { // make random key from number
			$pattern = "1234567890";
			$len = 10;
	  	}
		
		if( equalNum($type,'1') ) {
			$pattern = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // make random key from alphabet
			$len = 25;
		}

		if( equalNum($type,'2') ) {
			$pattern = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // make random key from mixed alphabet and number
			$len = 35;
		}
		
		for( $i = 0; $i < $length; $i++ ) {
			$param .= $pattern{ rand( 0, $len-1 ) };
		}
		
		return $param;
	}

	
	function nameFile( $param ) // get name from file
	{
		$param = pathinfo( $param );
        $param = $param['filename'];
		$param = str_replace( ".", "", $param );

		return $param;
	}

	function mimeType( $param ) // get mime type file
    {
        $file = pathinfo( $param );
		$ext = $file['extension'];
		$ext = strtolower($ext);

        switch($ext)
        {
        	// text
        	case "html" :
            case "htm" :
            case "php" :
                return "text/html";
        	
        	case "css" :
                return "text/css";
            
            case "txt" :
                return "text/plain";

            // application
            case "js" :
                return "application/x-javascript";
            
            case "json" :
                return "application/json";
            
            case "xml" :
                return "application/xml";
            
            case "swf" :
                return "application/x-shockwave-flash";

            // video
            case "flv" :
                return "video/x-flv";
            
            case "mpeg" :
            case "mpg" :
            case "mpe" :
                return "video/mpeg";

            case "mp4" :
                return "video/mp4";

            case "dv" :
            case "dif" :
                return "video/x-dv";

            case "mxu" :
            case "m4u" :
            	return "video/vnd.mpegurl";

            case "m4v" :
            	return "video/x-m4v";

            case "avi" :
                return "video/msvideo";
            
            case "wmv" :
                return "video/x-ms-wmv";

            case "movie" :
                return "video/x-sgi-movie";
            
            case "qt" :
            case "mov" :
                return "video/quicktime";
            
            // audio
            case "mpga" :
            case "mp2" :
            case "mp3" :
                return "audio/mpeg";

            case "ra" :
            case "ram" :
                return "audio/x-pn-realaudio";
            
            case "wav" :
                return "audio/wav";

            case "snd" :
            case "au" :
                return "audio/basic";

            case "m3u" :
                return "audio/x-mpegurl";

            case "m4a" :
            case "m4b" :
            case "m4p" :
                return "audio/mp4a-latm";

            case "mid" :
            case "midi" :
            case "kar" :
                return "audio/midi";
            
            case "aif" :
            case "aifc" :
            case "aiff" :
                return "audio/aiff";

            // image
            case "jpg" :
            case "jpeg" :
            case "jpe" :
                return "image/jpg";
            
            case "png" :
                return "image/png";

            case "gif" :
                return "image/gif";

            case "bmp" :
                return "image/bmp";

            case "ico" :
                return "image/vnd.microsoft.icon";

            case "svg" :
            case "svgz" :
                return "image/svg+xml";

            case "tif" :
            case "tiff" :
                return "image/tiff";

            // adobe
            case "pdf" :
                return "application/pdf";

            case "psd" :
                return "image/vnd.adobe.photoshop";

            case "ai" :
            case "eps" :
            case "ps" :
                return "video/postscript";

            // ms office
            case "doc" :
            case "docx" :
                return "application/msword";

            case "xls" :
            case "xlt" :
            case "xlm" :
            case "xld" :
            case "xla" :
            case "xlc" :
            case "xlw" :
            case "xll" :
                return "application/vnd.ms-excel";

            case "ppt" :
            case "pps" :
                return "application/vnd.ms-powerpoint";

            case "rtf" :
                return "application/rtf";

            // open office
            case "odt" :
                return "application/vnd.oasis.opendocument.text";

            case "ods" :
                return "application/vnd.oasis.opendocument.spreadsheet";

            // archives
            case "zip" :
                return "application/zip";

            case "msi" :
            case "exe" :
                return "application/x-msdownload";

            case "rar" :
                return "application/x-rar-compressed";

            case "cab" :
                return "application/vnd.ms-cab-compressed";

            case "tar" :
                return "application/x-tar";

            default :
            if(function_exists('mime_content_type'))
            {
                return mime_content_type($param);
            }else if(function_exists('finfo_open')){
            	$finfo = finfo_open(FILEINFO_MIME);
	            $param = finfo_file($finfo, $param);
	            finfo_close($finfo);
	            return $param;
            }else{
            	return "unknown/".$ext;
            }
        }
    }

    function findString($param, $find = '') // check if a string contains a character or a word
    {
    	$param = strtolower($param);
    	$find = strtolower($find);

    	if(strpos($param, $find) !== false){
    		return true;
    	}

    	return false;
    }

    function phpMailer($to, $from, $subject, $message, $cc = '', $bcc = '', $is_gmail = true)
	{
		//Create a new PHPMailer instance
		$mail = new PHPMailer();

		//Tell PHPMailer to use SMTP
		$mail->isSMTP();

		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		//$mail->SMTPDebug = 0;
		
		//Ask for HTML-friendly debug output
		//$mail->Debugoutput = 'html';
		
		if( $is_gmail ){
			//Set the hostname of the mail server			
			$mail->Host = 'master.breaktime.co.id';

			//Username to use for SMTP authentication			
			$mail->Username = "no-reply@breaktime.co.id";
			
			//Password to use for SMTP authentication			
			$mail->Password = "n0-r3ply";
			
		}else{
			//Set the hostname of the mail server
			$mail->Host = 'smtp.gmail.com';

			//Username to use for SMTP authentication
			$mail->Username = "mail@the-netwerk.com";

			//Password to use for SMTP authentication
			$mail->Password = "pastibisa100";
		}

		//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		$mail->Port = 587;

		//Set the encryption system to use - ssl (deprecated) or tls
		$mail->SMTPSecure = 'tls';

		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;

		//Set who the message is to be sent from
		foreach($from as $emailfrom => $namefrom)
		{
		   $mail->setFrom($emailfrom, $namefrom);
		}

		//Set who the message is to be sent to
		foreach($to as $emailto => $nameto)
		{
		   $mail->addAddress($emailto, $nameto);
		}

		//Set who the message is to be sent cc
		if( is_array( $cc ) )
		{
			foreach($cc as $emailcc => $namecc)
			{
			   $mail->addCC($emailcc, $namecc);
			}
		}
		

		//Set who the message is to be sent bcc
		if( is_array( $bcc ) )
		{
			foreach($bcc as $emailbcc => $namebcc)
			{
			   $mail->addBCC($emailbcc, $namebcc);
			}
		}
		
		//Set an alternative reply-to address
		//$mail->addReplyTo('replyto@example.com', 'First Last');

		//Set the subject line
		$mail->Subject = $subject;

		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
		$mail->msgHTML($message);

		//Replace the plain text body with one created manually
		//$mail->AltBody = 'Email test with php mailer';
		
		//Attach an image file
		//$mail->addAttachment('images/phpmailer_mini.png');
				
		//send the message, check for errors
		if($mail->send()) {
			return true;
		}

        return false;
	}

	function sendEmail($to, $from, $subject, $message, $type = '0', $cc = '', $bcc = ''){
	
		$headers = "MIME-Version: 1.0" . "\r\n";
		
		if ($type=="1") { // HTML			
			$headers .= "Content-type: text/html; charset=UTF-8" . "\r\n";
		} else { // plain text
			$headers .= "Content-type: text/plain; charset=iso-8859-1" . "\r\n";
		}
		
		// More headers
		$headers .= "From: ".$from. "\r\n";	
		
		if($cc!='') {$headers .= 'Cc: '.$cc.'\r\n';}	
		if($bcc!='') {$headers .= 'Bcc: '.$bcc.'\r\n';}
		
		$send = mail($to, $subject, $message, $headers);
		
		if($send){
			return true;
		} else {		
			return false;
		}
	}

	function email($to, $from, $subject, $message, $cc = '', $bcc = '')
	{
		$emailto = "";
		$emailfrom = "";
		$emailcc = "";
		$emailbcc = "";

		if(is_array($to)){
			$num = 0;
			foreach( $to as $email => $name ){
				if(equalNum($num,'0')){
					$emailto = $email;
				}else{
					$emailto = $emailto.",".$email;
				}
				$num++;					
			}
		}

		if(is_array($from)){
			$num = 0;
			foreach( $from as $email => $name ){
				if(equalNum($num,'0')){
					$emailfrom = $email;
				}else{
					$emailfrom = $emailfrom.",".$email;
				}
				$num++;					
			}
		}

		if(is_array($cc)){
			$num = 0;
			foreach( $cc as $email => $name ){
				if(equalNum($num,'0')){
					$emailcc = $email;
				}else{
					$emailcc = $emailcc.",".$email;
				}
				$num++;					
			}
		}

		if(is_array($bcc)){
			$num = 0;
			foreach( $bcc as $email => $name ){
				if(equalNum($num,'0')){
					$emailbcc = $email;
				}else{
					$emailbcc = $emailbcc.",".$email;
				}
				$num++;					
			}
		}

		/*$send = phpMailer($to, $from, $subject, $message, $cc, $bcc);
		if(!$send){
			$send = phpMailer($to, $from, $subject, $message, $cc, $bcc, false);
			if(!$send){*/
				$type = "1"; // html mail
				$send = sendEmail($emailto, $emailfrom, $subject, $message, $type, $emailcc, $emailbcc);
					
				if(!$send){
					return false;
				}
					
				return true;
			/*}
			
			return true;
		}
		return true;*/
	}
	function smsgateway($id, $phone, $code){
		// data
		$serviceSMS = "http://122.102.41.98:8081/maskxl/terima_cust.php";
		$custID = "S34I3"; // dev (Tes01)
		$custPass = "4phs96f96pgy"; // dev (zewb7v2xt8r6)
		$custPhone = $phone;
		$messageId = rand(100, 999).date("Y").date("m").date("d").date("H").date("i").date("s");
		$message = "Selamat Datang di Breaktime.co.id! Ikuti berbagai artikel yang lebih dekat dengan Anda. Login: http://bit.ly/regBT Silakan masukkan Kode: ".$code;
		
		// system
		$kolom = array(
			'CustID' => urlencode($custID),
			'CustPass' => urlencode($custPass),
			'Tujuan' => urlencode($custPhone),
			'Pesan' => urlencode($message),
			'MessageId' => urlencode($messageId),
		);
		$baris = "";
		foreach ($kolom as $kunci => $nilai) {
			$baris .= $kunci."=".$nilai."&";
		}

		// curl
		$saluran = curl_init();
		curl_setopt($saluran, CURLOPT_URL, $serviceSMS);
		curl_setopt($saluran, CURLOPT_POST, count($kolom));
		curl_setopt($saluran, CURLOPT_POSTFIELDS, substr($baris, 0, strlen($baris)-1));
		curl_setopt($saluran, CURLOPT_RETURNTRANSFER, true);
		$hasil = curl_exec($saluran);
		curl_close($saluran);

		// respond
		if(preg_match("/Remaining/i", $hasil)){
			remindertopup($hasil);
		}

		$status = "0"; // error
		if(preg_match("/SUCCESS/i", $hasil)){
			$status = "1"; // success
		}
		$query = "INSERT INTO sms_gateway 
        (messageid,memberid,phone,message,response) 
        VALUES 
        ('$messageId','$id','$phone','$message','$hasil')";
        mysqlQuery($query);
		return $status;
	}

	function smsgatewaycustom($id, $phone, $code){
		// data
		$serviceSMS = "http://122.102.41.98:8081/maskxl/terima_cust.php";
		$custID = "S34I3"; // dev (Tes01)
		$custPass = "4phs96f96pgy"; // dev (zewb7v2xt8r6)
		$custPhone = $phone;
		$messageId = rand(100, 999).date("Y").date("m").date("d").date("H").date("i").date("s");
		$message = "Selamat Datang di Breaktime.co.id! Ikuti berbagai artikel yang lebih dekat dengan Anda. Login: http://bit.ly/regBT No Undian kamu: ".$code;
		
		// system
		$kolom = array(
			'CustID' => urlencode($custID),
			'CustPass' => urlencode($custPass),
			'Tujuan' => urlencode($custPhone),
			'Pesan' => urlencode($message),
			'MessageId' => urlencode($messageId),
		);
		$baris = "";
		foreach ($kolom as $kunci => $nilai) {
			$baris .= $kunci."=".$nilai."&";
		}

		// curl
		$saluran = curl_init();
		curl_setopt($saluran, CURLOPT_URL, $serviceSMS);
		curl_setopt($saluran, CURLOPT_POST, count($kolom));
		curl_setopt($saluran, CURLOPT_POSTFIELDS, substr($baris, 0, strlen($baris)-1));
		curl_setopt($saluran, CURLOPT_RETURNTRANSFER, true);
		$hasil = curl_exec($saluran);
		curl_close($saluran);

		// respond
		if(preg_match("/Remaining/i", $hasil)){
			remindertopup($hasil);
		}

		$status = "0"; // error
		if(preg_match("/SUCCESS/i", $hasil)){
			$status = "1"; // success
		}
		$query = "INSERT INTO sms_gateway 
        (messageid,memberid,phone,message,response) 
        VALUES 
        ('$messageId','$id','$phone','$message','$hasil')";
        mysqlQuery($query);
		return $status;
	}

	function smsgatewayremarketing($id, $phone, $code){
		// data
		$serviceSMS = "http://122.102.41.98:8081/maskxl/terima_cust.php";
		$custID = "S1EBC"; // dev (Tes01)
		$custPass = "6v9a4xn1j0"; // dev (zewb7v2xt8r6)
		$custPhone = $phone;
		$messageId = rand(100, 999).date("Y").date("m").date("d").date("H").date("i").date("s");
		$message = "Terima kasih telah menjadi member Breaktime.co.id. Klaim NoUndian XIAOMI Mi4i kamu, klik: ".$code;
		
		// system
		$kolom = array(
			'CustID' => urlencode($custID),
			'CustPass' => urlencode($custPass),
			'Tujuan' => urlencode($custPhone),
			'Pesan' => urlencode($message),
			'MessageId' => urlencode($messageId),
		);
		$baris = "";
		foreach ($kolom as $kunci => $nilai) {
			$baris .= $kunci."=".$nilai."&";
		}

		// curl
		$saluran = curl_init();
		curl_setopt($saluran, CURLOPT_URL, $serviceSMS);
		curl_setopt($saluran, CURLOPT_POST, count($kolom));
		curl_setopt($saluran, CURLOPT_POSTFIELDS, substr($baris, 0, strlen($baris)-1));
		curl_setopt($saluran, CURLOPT_RETURNTRANSFER, true);
		$hasil = curl_exec($saluran);
		curl_close($saluran);

		// respond
		if(preg_match("/Remaining/i", $hasil)){
			remindertopup($hasil);
		}

		$status = "0"; // error
		if(preg_match("/SUCCESS/i", $hasil)){
			$status = "1"; // success
		}
		$query = "INSERT INTO sms_gateway 
        (messageid,memberid,phone,message,response) 
        VALUES 
        ('$messageId','$id','$phone','$message','$hasil')";
        mysqlQuery($query);
		return $status;
	}

	function smsgatewayremarketingundian($id, $phone, $code){
		// data
		$serviceSMS = "http://122.102.41.98:8081/maskxl/terima_cust.php";
		$custID = "S1EBC"; // dev (Tes01)
		$custPass = "6v9a4xn1j0"; // dev (zewb7v2xt8r6)
		$custPhone = $phone;
		$messageId = rand(100, 999).date("Y").date("m").date("d").date("H").date("i").date("s");
		$message = "Berikut nomor undian XIAOMI Mi4i kamu: ".$code;
		
		// system
		$kolom = array(
			'CustID' => urlencode($custID),
			'CustPass' => urlencode($custPass),
			'Tujuan' => urlencode($custPhone),
			'Pesan' => urlencode($message),
			'MessageId' => urlencode($messageId),
		);
		$baris = "";
		foreach ($kolom as $kunci => $nilai) {
			$baris .= $kunci."=".$nilai."&";
		}

		// curl
		$saluran = curl_init();
		curl_setopt($saluran, CURLOPT_URL, $serviceSMS);
		curl_setopt($saluran, CURLOPT_POST, count($kolom));
		curl_setopt($saluran, CURLOPT_POSTFIELDS, substr($baris, 0, strlen($baris)-1));
		curl_setopt($saluran, CURLOPT_RETURNTRANSFER, true);
		$hasil = curl_exec($saluran);
		curl_close($saluran);

		// respond
		if(preg_match("/Remaining/i", $hasil)){
			remindertopup($hasil);
		}

		$status = "0"; // error
		if(preg_match("/SUCCESS/i", $hasil)){
			$status = "1"; // success
		}
		$query = "INSERT INTO sms_gateway 
        (messageid,memberid,phone,message,response) 
        VALUES 
        ('$messageId','$id','$phone','$message','$hasil')";
        mysqlQuery($query);
		return $status;
	}

	function remindertopup($hasil){
		$r = explode(":",$hasil);
		$token = str_replace(" ", "", $r['1']);
		$token = str_replace(",", "", $token);
		if($token!="" && $token>=0){
			if($token<100){
				$messagecontent = "SMS Reminder Breaktime - Remaining Total Token: ".$token;
				smsgatewaycustommessage('0', '628123303168', $messagecontent);
			}
		}
	}

	function smsgatewaycustommessage($id, $phone, $messagecontent){
		// data
		$serviceSMS = "http://122.102.41.98:8081/maskxl/terima_cust.php";
		$custID = "S34I3"; // dev (Tes01)
		$custPass = "4phs96f96pgy"; // dev (zewb7v2xt8r6)
		$custPhone = $phone;
		$messageId = rand(100, 999).date("Y").date("m").date("d").date("H").date("i").date("s");
		$message = $messagecontent;
		
		// system
		$kolom = array(
			'CustID' => urlencode($custID),
			'CustPass' => urlencode($custPass),
			'Tujuan' => urlencode($custPhone),
			'Pesan' => urlencode($message),
			'MessageId' => urlencode($messageId),
		);
		$baris = "";
		foreach ($kolom as $kunci => $nilai) {
			$baris .= $kunci."=".$nilai."&";
		}

		// curl
		$saluran = curl_init();
		curl_setopt($saluran, CURLOPT_URL, $serviceSMS);
		curl_setopt($saluran, CURLOPT_POST, count($kolom));
		curl_setopt($saluran, CURLOPT_POSTFIELDS, substr($baris, 0, strlen($baris)-1));
		curl_setopt($saluran, CURLOPT_RETURNTRANSFER, true);
		$hasil = curl_exec($saluran);
		curl_close($saluran);

		// respond
		if(preg_match("/Remaining/i", $hasil)){
			remindertopup($hasil);
		}

		$status = "0"; // error
		if(preg_match("/SUCCESS/i", $hasil)){
			$status = "1"; // success
		}
		$query = "INSERT INTO sms_gateway 
        (messageid,memberid,phone,message,response) 
        VALUES 
        ('$messageId','$id','$phone','$message','$hasil')";
        mysqlQuery($query);
		return $status;
	}
?>