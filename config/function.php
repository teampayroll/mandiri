<?php
	/* Absolute path to the directory. */
	if ( !defined('CONPATH') ){
		define( 'CONPATH', dirname(dirname(__FILE__)) . '/' );
	}
	/* Absolute path to the directory. */
	
	require_once( CONPATH . 'config/aes.class.php' ); // AES PHP implementation
	require_once( CONPATH . 'config/aesctr.class.php' ); // AES Counter Mode implementation
	require_once( CONPATH . 'config/class.phpmailer.php' ); // PHP email class

	require_once( CONPATH . 'config/mysql.function.php' ); // This function contains the mysql
	require_once( CONPATH . 'config/general.function.php' ); // This function contains the general
	require_once( CONPATH . 'config/api.function.php' ); // This function contains the api data center

	require_once( CONPATH . 'config/connect.php' ); // This connect file
	require_once( CONPATH . 'config/dbconnect.php' ); // This dbconnect file
?>