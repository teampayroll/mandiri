<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "managesetting";
    $subpage = "edit";
    $page_name = "Setting System";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>
    
    <?php
        $statusinfo = 0;
        if (isset($_POST['type'])){
          $type= $_POST['type'];
          

          $query = "UPDATE mst_setting SET type = $type WHERE 1";
          if(mysqlQuery($query)){
            $statusinfo = '1';
            $successmessageinfo = 'Type System Berhasil Diganti';
          }else{
            $statusinfo = '2';
            $errormessageinfo = 'Type System Tidak Berhasil Diganti';
          }
        }

        $query = "SELECT *
                  FROM mst_setting";
        $result = mysqlQuery($query);
        if(mysqlNumRows($result)){
          while($row = mysqlFetchArray($result)){
            $type       = trim($row['type']);
            
          }
        }


    ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        
        <?php include('_nav.php'); ?>

        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mt_40">
            <?php  if($statusinfo == "1"){ ?>
              <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $successmessageinfo; ?></strong>
              </div>
            <?php } ?>
            <?php if($statusinfo == "2"){ ?>
              <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $errormessageinfo; ?></strong>
              </div>
            <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Manage Setting
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mb_40">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
                <form action="" method="POST">
                  <div class="row clearfix">
                    <div class="col-xs-12 mt_40">
                      <div class="form-group">
                        <label for="type" class="w_normal">Type System</label>
                        <select class="form-control br_0 select_blue" id="type" name="type">
                          <option value='0'<?php if($type == 0) echo 'selected' ?> >Sistem 1 Approver</option>
                          <option value='1' <?php if($type == 1) echo 'selected' ?> >Sistem 2 Approver</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-xs-12 mb_40">
                      <div class="right">
                        <button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Simpan</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        
        <?php include( PLUGPATH ."_footer.php"); ?>
        
      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
</html>
<?php ob_flush(); ?>