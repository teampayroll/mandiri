<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");


    $page = "managemember";
    $subpage = "";
    $page_name = "Manage Member";
    

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include( PLUGPATH ."_head.php"); ?>
    
    <?php
      $pnum=1;
      if(isset($_GET['pnum']))
      {
        $pnum = securefield($_GET['pnum'],5);
      }

      $statusinfo = 0;

      //Check after delete member
      if(isset($_GET['delete'])){
        if($_GET['delete'] == '1'){
          $statusinfo = 1;
          $successmessageinfo = "Member berhasil dihapus";
        }else if($_GET['delete'] == 2){
          $statusinfo = 2;
          $successmessageinfo = "Member gagal dihapus";
        }

        $_SESSION['status'] == 0;
      }

      //ADD MEMBER
      if( postExist('action') && equalPostStr('action','add') ){
        $namepost = secureLimit($_POST["name"],255);
        $emailpost = secureLimit($_POST["email"],255);
        $passwordpost = encryptIt(secureLimit($_POST["password"],255));
        $phonepost = trim($_POST["phone"]);
        $type = secureLimit($_POST["type"],255);

        $status_phone = "0";
        $provider[0]["name"]="TELKOMSEL";
        $provider[0]["code"]="TLKM";
        $provider[0]["pattern"]="/^(0|62)(811|812|813|821|822|823|851|852|853|8[4-5][0-9])/";
        $provider[0]["url"]="http://sms-api.jatismobile.com/index.ashx?";
        $provider[0]["url3355"]="http://192.168.114.11:11110/cgi-bin/sendsms?user=ecash&pass=3c4sh";

        $provider[1]["name"]="INDOSAT";
        $provider[1]["code"]="ISAT";
        $provider[1]["pattern"]="/^(0|62)(814|815|816|855|856|857|858)/";
        $provider[1]["url"]="http://sms-api.jatismobile.com/index.ashx?";
        $provider[1]["url3355"]="http://192.168.114.11:12110/cgi-bin/sendsms?user=ecash&pass=3c4sh";

        $provider[2]["name"]="EXCELCOM";
        $provider[2]["code"]="EXCL";
        $provider[2]["pattern"]="/^(0|62)(817|818|819|859|878|877|838|831|832|833)/";
        $provider[2]["url"]="http://sms-api.jatismobile.com/index.ashx?";
        $provider[2]["url3355"]="http://192.168.114.11:13110/cgi-bin/sendsms?user=ecash&pass=3c4sh";

        $provider[3]["name"]="ESIA";
        $provider[3]["pattern"]="/^((0|62)[236][124]2[1-9]|(0|62)[236][124]58|(0|62)[2-6][124]9[12345679]|(0|62)(232)|(0|62)(2358)|(0|62)(2391)|(0|62)(239)[256]|(0|62)[2-7][5-9]2|(0|62)[2-7][5-9]58|(0|62)[2-7][5-9]9[12567]|(0|62)[236][124]82|(0|62)(21514))[0-9]{5,9}/";
        $provider[3]["url"]="http://sms-api.jatismobile.com/index.ashx?";
        $provider[3]["url3355"]="http://192.168.114.11:15100/cgi-bin/sendsms?user=ecash&pass=3c4sh";

        $provider[4]["name"]="FLEXI";
        $provider[4]["pattern"]="/^((62|0)[2-6][124]20|(62|0)21279|(62|0)[2-6][1-4]31|(62|0)[2-7][1-9]3[01]|(62|0)[2-6][1-4]3[4-7]|(62|0)[2-7][1-9]3[3-7]|(62|0)[2-6][1-4]41|(62|0)[2-7][1-9]41|(62|0)215[023]|(62|0)[2-6][2-4]5[0-3]|(62|0)[2-7][2-9]5[0-3]|(62|0)[2-6][1-4]59|(62|0)[2-7][1-9]5[59]|(62|0)[2-6][1-4]6[348]|(62|0)[2-7][1-9]6[468]|(62|0)[2-6][1-4]7|(62|0)[2-7][1-9][1-8]7|(62|0)[2-6][1-4]8[13456789]|(62|0)[2-7][1-9]8|(62|0)[2-6][1-4]94|(62|0)[2-7][1-9]94|(62|0)[2-7][1-9]40|(62|0)[2-7][5-9]91|(62|0)21288)[0-9]{5,9}/";
        $provider[4]["url"]="http://sms-api.jatismobile.com/index.ashx?";
        $provider[4]["url3355"]="http://192.168.114.11:14100/cgi-bin/sendsms?user=ecash&pass=3c4sh";

        $provider[5]["name"]="SMART";
        $provider[5]["pattern"]="/^(62881|0881|0881|62881|62882|0882|0882|62882|62883|0883|0883|62883|62884|0884|0884|62884|62885|0885|0885|62885|62886|0886|0886|62886|62887|0887|0887|62887|62888|0888|0888|62888|62889|0889|0889|62889)[0-9]{5,9}/";
        $provider[5]["url"]="http://sms-api.jatismobile.com/index.ashx?";
        $provider[5]["url3355"]="http://192.168.114.11:16100/cgi-bin/sendsms?user=ecash&pass=3c4sh";

        if(preg_match($provider[0]["pattern"], $phonepost)){
          $status_phone = "1";
        }else if(preg_match($provider[1]["pattern"], $phonepost)){
          $status_phone = "1";
        }else if(preg_match($provider[2]["pattern"], $phonepost)){
          $status_phone = "1";
        }else if(preg_match($provider[3]["pattern"], $phonepost)){
          $status_phone = "1";
        }else if(preg_match($provider[4]["pattern"], $phonepost)){
          $status_phone = "1";
        }else if(preg_match($provider[5]["pattern"], $phonepost)){
          $status_phone = "1";
        }

        $status_team = "0";
        if($type == 1){
          if( postExist('team') ){
            $team = secureLimit($_POST["team"],255);
          }else{
            $status_team = "1";
          }
        }else{
          $team = 0;
        }

        $query_select = "SELECT id FROM mst_user WHERE email = '$emailpost' AND status = '0'";
        $result_select = mysqlQuery($query_select);
        if(!mysqlNumRows($result_select)){
          if($status_team=="0" && $status_phone=="1"){
            $query3 = "INSERT INTO mst_user 
              (name,email,password,phone,type,
                team, status, createdby,createddate,updatedby, updateddate) 
              VALUES 
              ('$namepost','$emailpost','$passwordpost','$phonepost', '$type',
                '$team', '0', '$adminId', '$datepost', '$adminId', '$datepost')";
            if(mysqlQuery($query3)){
              $statusinfo = '1'; //Successfuly inserted
              $successmessageinfo = "Member berhasil ditambahkan";
            }else{
              $statusinfo = '2'; //Failed
              $errormessageinfo = "Maaf, Member gagal ditambahkan";
            }

            // GET USER INDEX ID
            $query2 = "SELECT LAST_INSERT_ID() FROM mst_user;";
            $result2 = mysqlQuery($query2);
            while($row2 = mysqlFetchArray($result2)){
                $id = trim($row2['LAST_INSERT_ID()']);
            }

            if($type == 0){
              $query = "INSERT INTO tr_useraccess VALUES 
                  ($id, 'bulk update',1),
                  ($id, 'schedule',2),
                  ($id, 'history activity',3),
                  ($id, 'history otorisasi',4),
                  ($id, 'edit profile',5)";
              mysqlQuery($query);
            }else  if($type == 1){
              $query = "INSERT INTO tr_useraccess VALUES 
                  ($id, 'notification',6),
                  ($id, 'history activity',3),
                  ($id, 'history otorisasi',4),
                  ($id, 'edit profile',5)";
              mysqlQuery($query);
            }else{
              $query = "INSERT INTO tr_useraccess VALUES 
                  ($id, 'manage user',7),
                  ($id, 'manage access',8),
                  ($id, 'manage setting',9),
                  ($id, 'edit profile',5)";
              mysqlQuery($query);
            }
          }else{
            if($status_team=="1"){
              $statusinfo = "2";
              $errormessageinfo = "Maaf, Team dari approver harus dipilih.";
            }else{
              $statusinfo = "2";
              $errormessageinfo = "Maaf, No handphone anda tidak valid.";
            }
          }
        }else{
          $statusinfo = "2";
          $errormessageinfo = "Maaf, Email yang akan anda gunakan sudah tersimpan pada sistem.";
        }mysqlFreeResult($result_select);
      }
    ?>

  </head>

   <style>
    .hidden{
      display: none;
    }
  </style>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        
        <?php include('_nav.php'); 
        ?>

        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-6 col-md-8">
              <div class="row clearfix">
                <div class="col-xs-12 mt_40">
                <?php if($statusinfo=="1"){ ?>
                  <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong><?php echo $successmessageinfo; ?></strong>
                  </div>
                <?php } ?>
                <?php if($statusinfo=="2"){ ?>
                  <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong><?php echo $errormessageinfo; ?></strong>
                  </div>
                <?php } ?>
                </div>
                <div class="col-xs-12">
                  <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                    Manage Member
                  </div>
                </div>
                <div class="col-xs-12">
                  <div class="bg_white c_blue fs_14">
                    <div class="table-responsive">
                      <table id="wrap_table" class="table">
                        <thead>
                          <tr>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Type</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                            // set default
                            $recordperpages = 10;
                            $targetpage = "manage_member.php";
                            
                            $startpages = ($pnum-1)*$recordperpages;
                            $query2 = "SELECT * FROM mst_user WHERE status != 2 AND id != '$adminId' ";
                            $result = mysqlQuery($query2);
                            $totalrecords = mysqlNumRows($result);

                            $query2 = $query2."ORDER BY id DESC LIMIT $startpages,$recordperpages";
                            $result = mysqlQuery($query2);
                            if(mysqlNumRows($result)){
                              while($row = mysqlFetchArray($result)){
                                $id       = trim($row['id']);
                                $name     = trim($row['name']);
                                $email    = trim($row['email']);
                                $phone    = trim($row['phone']);
                                $type     = trim($row['type']);
                          ?>
                          <tr>
                            <td><?php echo $name ?></td>
                            <td><?php echo $email ?></td>
                            <td><?php echo $phone ?></td>
                            <td>
                                <?php 
                                  if($type == 0){
                                    echo "Submitter";
                                  }else if($type == 1){
                                    echo "Approver";
                                  }else{
                                    echo "Superadmin";
                                  }

                                  ?>
                            </td>
                            <td class="action">
                              <a href="edit_profile.php?id=<?php echo $id ?>" class="mr_10"><img src="<?php echo siteUrl(); ?>images/icon_edit.png" /></a>
                              <a data-toggle="modal" data-target="#deletePopUp<?php echo $id?>" type="submit"><img src="<?php echo siteUrl(); ?>images/icon_del.png" /></a>
                               <div class="modal fade popup" id="deletePopUp<?php echo $id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog modal-sm" role="document">
                                  <form method="POST" action="delete_member.php">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      </div>
                                      <div class="modal-body">
                                        Are you sure want to delete this member?
                                      </div>
                                      <div class="modal-footer">
                                        <input type="text" name="deletedId" class="hidden" value="<?php echo $id ?>">
                                        <button  class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Delete</button>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <?php
                              }
                            }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 mb_40">
                  <div id="wrap_page" class="right cs_df">
                  <?php
                    echo generatePagination($totalrecords, $recordperpages, $targetpage, $pnum, "pnum", 2);
                  ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="row clearfix">
                <div class="col-xs-12 mt_40">
                  <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                    Add Member
                  </div>
                </div>
                <div class="col-xs-12 mb_40">
                  <div class="bg_white pl_20 pr_20 c_blue fs_14">
                    <form data-parsley-validate method="POST" action="">
                      <div class="row clearfix">
                        <div class="col-xs-12 mt_40">
                          <div class="form-group">
                            <label for="name" class="w_normal">Name</label>
                            <input required type="text" class="form-control br_0 bg_white c_grey" id="name" name="name" value="">
                          </div>
                          <div class="form-group">
                            <label for="email" class="w_normal">Email</label>
                            <input required data-parsley-type="email" type="email" class="form-control br_0 bg_white c_grey" id="email" name="email" value="">
                          </div>
                          <div class="form-group">
                            <label for="password" class="w_normal">Password</label>
                            <input required type="password" class="form-control br_0 bg_white c_grey" id="password" name="password" value="">
                          </div>
                          <div class="form-group">
                            <label for="phone" class="w_normal">Phone</label>
                            <input required type="text" class="form-control br_0 bg_white c_grey" id="phone" name="phone" value="">
                          </div>
                          <div class="form-group">
                            <label for="type" class="w_normal">Type</label>
                            <select required class="form-control br_0 select_blue" id="type" name="type">
                              <option  disabled selected hidden >Choose Type</option>
                              <option value = "0">Submitter</option>
                              <option value = "1">Approver</option>
                              <option value = "2">Superadmin</option>
                            </select>
                          </div>
                          <div class="form-group" id="input-team">
                            <label for="team" class="w_normal">Team</label>
                            <select class="form-control br_0 select_blue" id="team" name="team">
                              <option disabled selected hidden >Choose Team</option>
                              <option value = "1">A</option>
                              <option value = "2">B</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-xs-12 mb_40">
                          <div class="right">
                            <button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Add</button>
                          </div>
                        </div>
                      </div>
                       <input type="hidden" name="action" value="add" /><!-- action add -->
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <?php include( PLUGPATH ."_footer.php"); ?>
        
      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>

  <script>  

    //Option Select Team
    $('#input-team').hide();
    $('#type').on('change', function (e) {
      var optionSelected = $("option:selected", this);
      var valueSelected = this.value;
      if(valueSelected == 1){
        $('#input-team').show();
         $('#input-team').attr('required','');
      }else{
         $('#input-team').hide();
         $('#input-team').removeAttr('required');
      }
    });

    $('.submit').click(function(){
      $(this).parent().find('button').trigger("click");
    });

  </script>

</html>
<?php ob_flush(); ?>