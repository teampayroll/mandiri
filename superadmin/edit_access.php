<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "manageaccess";
    $subpage = "edit";
    $page_name = "Edit Access";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>

    <?php
      $statusinfo = 0;
      if( getExist('id') && !equalGetStr('id','') ){
        $id = $_GET['id'];
        $query = "SELECT *
                FROM mst_user
                WHERE id = $id";
        $result = mysqlQuery($query);
        if(mysqlNumRows($result)){
          while($row = mysqlFetchArray($result)){
            $name = trim($row['name']);
            $type = trim($row['type']);
          }
        }
      }

      if( postExist('action') && equalPostStr('action','edit') ){
        
        //Delete records from table first
        $query ="DELETE FROM tr_useraccess WHERE iduser = $id";
        mysqlQuery($query);

        //Insert new record into database
         $query = "INSERT INTO tr_useraccess VALUES ";
        if(isset($_POST['cb1'])){
          $query= $query. "($id, 'bulk update', 1), ";
        }
        if(isset($_POST['cb2'])){
          $query= $query. "($id, 'bulk schedule', 2), ";
        }
        if(isset($_POST['cb3'])){
          $query= $query. "($id, 'history activity', 3), ";
        }
        if(isset($_POST['cb4'])){
          $query= $query. "($id, 'history otorisasi', 4), ";
        }
        if(isset($_POST['cb5'])){
          $query= $query. "($id, 'edit profile', 5), ";
        }
        if(isset($_POST['cb6'])){
          $query= $query. "($id, 'notifications', 6), ";
        }
        if(isset($_POST['cb7'])){
          $query= $query. "($id, 'manage user', 7), ";
        }
        if(isset($_POST['cb8'])){
          $query= $query. "($id, 'manage access', 8), ";
        }
        if(isset($_POST['cb9'])){
          $query= $query. "($id, 'manage setting', 9), ";
        }
        
        $query = trim($query, ", ");
        if(mysqlQuery($query)){
          $statusinfo = "1"; //Successfuly inserted
          $successmessageinfo = "Edit access anda berhasil";
        }else{
          $statusinfo = "2"; //Successfuly inserted
          $successmessageinfo = "Edit access anda gagal";
        }

      }
      


      $access = ['','','','','','','','','',''];
      $query = "SELECT *
                  FROM tr_useraccess
                  WHERE iduser = $id";
      $result = mysqlQuery($query);
      if(mysqlNumRows($result)){
        while($row = mysqlFetchArray($result)){
          $idaccess     = trim($row['idaccess']);
          $access[$idaccess] = 'checked';
        }
      }

    ?>
    
  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        
        <?php include('_nav.php'); ?>

        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 mt_40">
            <?php  if($statusinfo == "1"){ ?>
              <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $successmessageinfo; ?></strong>
              </div>
            <?php } ?>
            <?php if($statusinfo == "2"){ ?>
              <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $errormessageinfo; ?></strong>
              </div>
            <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
              <div class="row clearfix">
                <div class="col-xs-6">
                  <div class="fs_14 bold c_blue2 left"><?php echo $name ?></div>
                </div>
                <div class="col-xs-6">
                  <div class="fs_14 bold c_blue2 right">
                    <?php 
                      if($type == 0){
                        echo "Submitter";
                      }else if( $type == 1 ){
                        echo "Approver";
                      }else{
                        echo "Superadmin";
                      }

                    ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Control Page
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 mb_40">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
                <form method="POST" action="">
                  <div class="row clearfix">
                    <div class="col-xs-12 mt_40">

                      <?php 
                      if($type == 0){
                      ?>
                      <div class="checkbox mb_30">
                        <label>
                          <input type="checkbox" id="" name="cb1" <?php echo $access[1] ?> > Bulk Update
                        </label>
                      </div>
                      <div class="checkbox mb_30">
                        <label>
                          <input type="checkbox" id="" name="cb2"  <?php echo $access[2] ?>> Schedule
                        </label>
                      </div>
                      <div class="checkbox mb_30">
                        <label>
                          <input type="checkbox" id="" name="cb3" <?php echo $access[3] ?>> History Activity
                        </label>
                      </div>
                      <div class="checkbox mb_30">
                        <label>
                          <input type="checkbox" id="" name="cb4" <?php echo $access[4] ?>> History Otorisasi
                        </label>
                      </div>
                      <div class="checkbox mb_30">
                        <label>
                          <input type="checkbox" id="" name="cb5" <?php echo $access[5] ?>> Edit Profile
                        </label>
                      </div>
                      <?php 
                          } else if($type == 1){ 
                        ?>
                        <div class="checkbox mb_30">
                          <label>
                            <input type="checkbox" id="" name="cb6" <?php echo $access[6] ?>> Notifications
                          </label>
                        </div>
                        <div class="checkbox mb_30">
                          <label>
                            <input type="checkbox" id="" name="cb3" <?php echo $access[3] ?>> History Activity
                          </label>
                        </div>
                        <div class="checkbox mb_30">
                          <label>
                            <input type="checkbox" id="" name="cb4" <?php echo $access[4] ?>> History Otorisasi
                          </label>
                        </div>
                        <div class="checkbox mb_30">
                          <label>
                            <input type="checkbox" id="" name="cb5" <?php echo $access[5] ?>> Edit Profile
                          </label>
                        </div>
                      <?php 
                        }else{ 
                      ?>
                         <div class="checkbox mb_30">
                          <label>
                            <input type="checkbox" id="" name="cb7" <?php echo $access[7] ?>> Manage User
                          </label>
                        </div>
                        <div class="checkbox mb_30">
                          <label>
                            <input type="checkbox" id="" name="cb8" <?php echo $access[8] ?>> Manage Access
                          </label>
                        </div>
                        <div class="checkbox mb_30">
                          <label>
                            <input type="checkbox" id="" name="cb9" <?php echo $access[9] ?>> Manage Setting
                          </label>
                        </div>
                        <div class="checkbox mb_30">
                          <label>
                            <input type="checkbox" id="" name="cb5" <?php echo $access[5] ?>> Edit Profile
                          </label>
                        </div>
                      <?php } ?>
                    </div>
                    <div class="col-xs-12 mb_40">
                      <div class="right">
                        <a href="manage_access.php"><button type="button" id="button_cancel" class="btn btn-default button_white fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Cancel</button></a>
                        <button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Save</button>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="action" value="edit" />
                  <input type="hidden" name="id" value="<?php echo $id?>" />
                </form>
              </div>
            </div>
          </div>
        </div>
        
        <?php include( PLUGPATH ."_footer.php"); ?>

      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
  </body>
</html>
<?php ob_flush(); ?>