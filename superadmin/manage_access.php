<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "manageaccess";
    $subpage = "";
    $page_name = "Manage Access";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>

    <?php
      $pnum=1;
      if(isset($_GET['pnum']))
      {
        $pnum = securefield($_GET['pnum'],5);
      }
    ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">

        <?php include('_nav.php'); ?>
        
        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Manage Access
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
              <div class="bg_white c_blue fs_14">
                <div class="table-responsive">
                  <table id="wrap_table" class="table">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Type</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                          // set default
                          $recordperpages = 10;
                          $targetpage = "manage_access.php";
                          
                          $startpages = ($pnum-1)*$recordperpages;
                          $query2 = "SELECT * FROM mst_user WHERE status != 2 ";
                          $result = mysqlQuery($query2);
                          $totalrecords = mysqlNumRows($result);

                          $query2 = $query2."ORDER BY id DESC LIMIT $startpages,$recordperpages";
                          $result = mysqlQuery($query2);
                          if(mysqlNumRows($result)){
                            while($row = mysqlFetchArray($result)){
                              $id       = trim($row['id']);
                              $name     = trim($row['name']);
                              $email    = trim($row['email']);
                              $phone    = trim($row['phone']);
                              $type     = trim($row['type']);
                        ?>
                      <tr>
                        <td><?php echo $name ?></td>
                        <td><?php echo $email ?></td>
                        <td><?php echo $phone ?></td>
                         <td>
                                <?php 
                                  if($type == 0){
                                    echo "Submitter";
                                  }else if($type == 1){
                                    echo "Approver";
                                  }else{
                                    echo "Superadmin";
                                  }

                                  ?>
                            </td>
                        <td class="action">
                          <a href="edit_access.php?id=<?php echo $id ?>"><img src="<?php echo siteUrl(); ?>images/icon_edit.png" /></a>
                        </td>
                      </tr>
                      <?php
                        }
                      }
                      ?>
                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mb_40">
              <div id="wrap_page" class="right cs_df">
              <?php
                echo generatePagination($totalrecords, $recordperpages, $targetpage, $pnum, "pnum", 2);
              ?>
              </div>
            </div>
          </div>
        </div>
        
        <?php include( PLUGPATH ."_footer.php"); ?>

      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
</html>
<?php ob_flush(); ?>