<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "profile";
    $subpage = "edit";
    $page_name = "Edit Profile";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>
    
    <?php
      $statusinfo = 0;
      $id = $adminId;
      $query = "SELECT *
              FROM mst_user
              WHERE id = $id";
      $result = mysqlQuery($query);
      if(mysqlNumRows($result)){
        while($row = mysqlFetchArray($result)){
          $id       = trim($row['id']);
          $name     = trim($row['name']);
          $password = decryptIt(trim($row['password']));
          $email    = trim($row['email']);
          $phone    = trim($row['phone']);
          $type     = trim($row['type']);
          $team     = trim($row['team']);
        }
      }
     

      //UPDATE PROFILE
      if( postExist('action') && equalPostStr('action','edit') ){
          $id = secureLimit($_POST["id"],255);
          $namepost = secureLimit($_POST["name"],255);
          $emailpost = secureLimit($_POST["email"],255);
          $old_password = secureLimit($_POST["old_password"],255);
          $new_password = secureLimit($_POST["new_password"],255);
          $phonepost = trim($_POST["phone"]);
          $typepost = secureLimit($_POST["type"],255);

          $status_phone = "0";
          $provider[0]["name"]="TELKOMSEL";
          $provider[0]["code"]="TLKM";
          $provider[0]["pattern"]="/^(0|62)(811|812|813|821|822|823|851|852|853|8[4-5][0-9])/";
          $provider[0]["url"]="http://sms-api.jatismobile.com/index.ashx?";
          $provider[0]["url3355"]="http://192.168.114.11:11110/cgi-bin/sendsms?user=ecash&pass=3c4sh";

          $provider[1]["name"]="INDOSAT";
          $provider[1]["code"]="ISAT";
          $provider[1]["pattern"]="/^(0|62)(814|815|816|855|856|857|858)/";
          $provider[1]["url"]="http://sms-api.jatismobile.com/index.ashx?";
          $provider[1]["url3355"]="http://192.168.114.11:12110/cgi-bin/sendsms?user=ecash&pass=3c4sh";

          $provider[2]["name"]="EXCELCOM";
          $provider[2]["code"]="EXCL";
          $provider[2]["pattern"]="/^(0|62)(817|818|819|859|878|877|838|831|832|833)/";
          $provider[2]["url"]="http://sms-api.jatismobile.com/index.ashx?";
          $provider[2]["url3355"]="http://192.168.114.11:13110/cgi-bin/sendsms?user=ecash&pass=3c4sh";

          $provider[3]["name"]="ESIA";
          $provider[3]["pattern"]="/^((0|62)[236][124]2[1-9]|(0|62)[236][124]58|(0|62)[2-6][124]9[12345679]|(0|62)(232)|(0|62)(2358)|(0|62)(2391)|(0|62)(239)[256]|(0|62)[2-7][5-9]2|(0|62)[2-7][5-9]58|(0|62)[2-7][5-9]9[12567]|(0|62)[236][124]82|(0|62)(21514))[0-9]{5,9}/";
          $provider[3]["url"]="http://sms-api.jatismobile.com/index.ashx?";
          $provider[3]["url3355"]="http://192.168.114.11:15100/cgi-bin/sendsms?user=ecash&pass=3c4sh";

          $provider[4]["name"]="FLEXI";
          $provider[4]["pattern"]="/^((62|0)[2-6][124]20|(62|0)21279|(62|0)[2-6][1-4]31|(62|0)[2-7][1-9]3[01]|(62|0)[2-6][1-4]3[4-7]|(62|0)[2-7][1-9]3[3-7]|(62|0)[2-6][1-4]41|(62|0)[2-7][1-9]41|(62|0)215[023]|(62|0)[2-6][2-4]5[0-3]|(62|0)[2-7][2-9]5[0-3]|(62|0)[2-6][1-4]59|(62|0)[2-7][1-9]5[59]|(62|0)[2-6][1-4]6[348]|(62|0)[2-7][1-9]6[468]|(62|0)[2-6][1-4]7|(62|0)[2-7][1-9][1-8]7|(62|0)[2-6][1-4]8[13456789]|(62|0)[2-7][1-9]8|(62|0)[2-6][1-4]94|(62|0)[2-7][1-9]94|(62|0)[2-7][1-9]40|(62|0)[2-7][5-9]91|(62|0)21288)[0-9]{5,9}/";
          $provider[4]["url"]="http://sms-api.jatismobile.com/index.ashx?";
          $provider[4]["url3355"]="http://192.168.114.11:14100/cgi-bin/sendsms?user=ecash&pass=3c4sh";

          $provider[5]["name"]="SMART";
          $provider[5]["pattern"]="/^(62881|0881|0881|62881|62882|0882|0882|62882|62883|0883|0883|62883|62884|0884|0884|62884|62885|0885|0885|62885|62886|0886|0886|62886|62887|0887|0887|62887|62888|0888|0888|62888|62889|0889|0889|62889)[0-9]{5,9}/";
          $provider[5]["url"]="http://sms-api.jatismobile.com/index.ashx?";
          $provider[5]["url3355"]="http://192.168.114.11:16100/cgi-bin/sendsms?user=ecash&pass=3c4sh";

          if(preg_match($provider[0]["pattern"], $phonepost)){
            $status_phone = "1";
          }else if(preg_match($provider[1]["pattern"], $phonepost)){
            $status_phone = "1";
          }else if(preg_match($provider[2]["pattern"], $phonepost)){
            $status_phone = "1";
          }else if(preg_match($provider[3]["pattern"], $phonepost)){
            $status_phone = "1";
          }else if(preg_match($provider[4]["pattern"], $phonepost)){
            $status_phone = "1";
          }else if(preg_match($provider[5]["pattern"], $phonepost)){
            $status_phone = "1";
          }

          if($typepost == 1){
            $team = secureLimit($_POST["team"],255);
          }else{
            $team = 0;
          }

          if($status_phone=="1"){
            $query = "UPDATE mst_user SET name = '$namepost', email = '$emailpost', phone = '$phonepost', type = $typepost, team = $team WHERE id = $id";
            if(mysqlQuery($query)){
              $statusinfo = "1"; //Successfuly inserted
              $successmessageinfo = "Edit profile anda berhasil";

              $_SESSION['admin'] = $namepost;
              $_SESSION['email'] = $emailpost;
              $_SESSION['type'] = $typepost;
            }else{
              $statusinfo = "2"; //Failed
              $successmessageinfo = "Edit profile anda gagal";
            }

            //UPDATE table tr_useraccess
            if($typepost != $type){
              $query ="DELETE FROM tr_useraccess WHERE iduser = $id";
              mysqlQuery($query);

              if($typepost == 0){
              $query = "INSERT INTO tr_useraccess VALUES 
                  ($id, 'bulk update',1),
                  ($id, 'schedule',2),
                  ($id, 'history activity',3),
                  ($id, 'history otorisasi',4),
                  ($id, 'edit profile',5)";
              mysqlQuery($query);
              }else  if($typepost == 1){
                $query = "INSERT INTO tr_useraccess VALUES 
                    ($id, 'notification',6),
                    ($id, 'history activity',3),
                    ($id, 'history otorisasi',4),
                    ($id, 'edit profile',5)";
                mysqlQuery($query);
              }else{
                $query = "INSERT INTO tr_useraccess VALUES 
                    ($id, 'manage user',7),
                    ($id, 'manage access',8),
                    ($id, 'manage setting',9),
                    ($id, 'edit profile',5)";
                mysqlQuery($query);
              }
            }

            $name = $namepost;
            $email = $emailpost;
            $phone = $phonepost;
            $type = $typepost;

            //CHange Password
            if($old_password!="" || $new_password !=""){
              if($old_password==""){
                $statusinfo = "2";
                $errormessageinfo = "Maaf, Password lama harus di isi.";
              }else if($new_password==""){
                $statusinfo = "2";
                $errormessageinfo = "Maaf, Password baru harus di isi.";
              }else if($old_password==$password){
                $new_password = encryptIt($new_password);
                $query = "UPDATE mst_user SET password = '$new_password', updatedby = '$adminId', updateddate = '$datepost', ipaddress = '$ipaddresspost' WHERE id = '$adminId'";
                if(mysqlQuery($query)){
                  $statusinfo = "1";
                  $successmessage = "Edit profile anda berhasil.";
                }else{
                  $statusinfo = "2";
                  $errormessageinfo = "Maaf, Edit profile anda tidak berhasil.";
                }
              }else{
                $statusinfo = "2";
                $errormessageinfo = "Maaf, Password lama anda tidak sesuai.";
              }
            }
          }else{
            $statusinfo = "2";
            $errormessageinfo = "Maaf, No handphone anda tidak valid.";
          }
       }
    ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        <?php include('_nav.php');  

        ?>

        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mt_40">
            <?php  if($statusinfo == "1"){ ?>
              <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $successmessageinfo; ?></strong>
              </div>
            <?php } ?>
            <?php if($statusinfo == "2"){ ?>
              <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $errormessageinfo; ?></strong>
              </div>
            <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Edit Profile
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mb_40">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
                <form method="POST" action="">
                  <div class="row clearfix">
                    <div class="col-xs-12 mt_40">
                      <div class="form-group">
                        <label for="name" class="w_normal">Name</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="name" name="name" value="<?php echo $name?>">
                      </div>
                      <div class="form-group">
                        <label for="email" class="w_normal">Email</label>
                        <input type="email" class="form-control br_0 bg_white c_grey" id="email" name="email" value="<?php echo $email?>">
                      </div>
                      <div class="form-group">
                        <label for="old_password" class="w_normal">Password Lama</label>
                        <input type="password" class="form-control br_0 bg_white c_grey" id="old_password" name="old_password" value="">
                      </div>
                      <div class="form-group">
                        <label for="new_password" class="w_normal">Password Baru</label>
                        <input type="password" class="form-control br_0 bg_white c_grey" id="new_password" name="new_password" value="">
                      </div>
                      <div class="form-group">
                        <label for="phone" class="w_normal">Phone</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="phone" name="phone" value="<?php echo $phone?>">
                      </div>
                      <div class="form-group">
                        <label for="type" class="w_normal">Type</label>
                        <select class="form-control br_0 select_blue" id="type" name="type">
                          <option value="0" <?php if($type==0) echo 'selected' ?>>Submitter</option>
                          <option value="1" <?php if($type==1) echo 'selected' ?>>Approver</option>
                          <option value="2" <?php if($type==2) echo 'selected' ?>>Superadmin</option>
                        </select>
                      </div>
                      <div class="form-group" id="input-team">
                        <label for="team" class="w_normal">Team</label>
                        <select class="form-control br_0 select_blue" id="team" name="team">
                          <option <?php if($team==1) echo 'selected' ?> value="1">A</option>
                          <option <?php if($team==2) echo 'selected' ?> value="2">B</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-xs-12 mb_40">
                      <div class="right">
                        <a href="profile.php"> <button type="button" id="button_cancel" class="btn btn-default button_white fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Batalkan</button></a>
                        <button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Simpan</button>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="action" value="edit" />
                  <input type="hidden" name="id" value="<?php echo $id?>" />
                </form>
              </div>
            </div>
          </div>
        </div>
        
        <?php include( PLUGPATH ."_footer.php"); ?>
        
      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
  <script>

    if($('#type').val() != "1"){
      $('#input-team').hide();
    }
    
    $('#type').on('change', function (e) {
      var optionSelected = $("option:selected", this);
      var valueSelected = this.value;
      if(valueSelected == 1){
        $('#input-team').show();
         $('#input-team').attr('required','');
      }else{
         $('#input-team').hide();
         $('#input-team').removeAttr('required');
      }
    });

  </script>
</html>
<?php ob_flush(); ?>