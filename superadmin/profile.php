<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "profile";
    $subpage = "profile";
    $page_name = "Profile";
?>
<!DOCTYPE html>
<html lang="en">
  <head>

    <?php include( PLUGPATH ."_head.php"); ?>

    <?php        
        $id = $adminId;
        
        $query = "SELECT *
                FROM mst_user
                WHERE id = $id";
        $result = mysqlQuery($query);
        if(mysqlNumRows($result)){
          while($row = mysqlFetchArray($result)){
            $id       = trim($row['id']);
            $name     = trim($row['name']);
            $passwordChar = strlen(trim($row['password']));
            $email    = trim($row['email']);
            $phone    = trim($row['phone']);
            $type     = trim($row['type']);
            $team     = trim($row['team']);
          }
        }
      
    ?>
    
  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        
        <?php include('_nav.php'); ?>

        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Profile
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mb_40">
              <div class="bg_white pl_20 pr_20 fs_14">
                  <div class="row clearfix">
                    <div class="col-xs-12 col-sm-4 mt_40 c_blue">
                      Name
                    </div>
                    <div class="col-xs-12 col-sm-8 mt_40 c_grey">
                      <?php echo $name ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 mt_40 c_blue">
                      Email
                    </div>
                    <div class="col-xs-12 col-sm-8 mt_40 c_grey">
                      <?php echo $email ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 mt_40 c_blue">
                      Password
                    </div>
                    <div class="col-xs-12 col-sm-8 mt_40 c_grey">
                      ******
                    </div>
                    <div class="col-xs-12 col-sm-4 mt_40 c_blue">
                      Phone
                    </div>
                    <div class="col-xs-12 col-sm-8 mt_40 c_grey">
                      <?php echo $phone ?>
                    </div>
                    <div class="col-xs-12 mt_20 mb_40">
                      <div class="right">
                        <a href="profile_edit.php">
                          <button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Ubah</button>
                        </a>
                      </div>
                    </div>
                  </div>
                
              </div>
            </div>
          </div>
        </div>

        <?php include( PLUGPATH ."_footer.php"); ?>
        
      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
</html>
<?php ob_flush(); ?>