<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "bulkupdate";
    $subpage = "";
    $page_name = "Bulk Update Member";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>

    <?php
      if( postExist('action') && equalPostStr('action','update')){
        $desc = secureText($_POST["keterangan_update"]);

        $type = "0";

        $uploaddir = fileUpload('lib');

        $filepost = "";
        if( !equalStr($_FILES['file_update']['name'],'') )
        {
          $filename = secureFile($_FILES['file_update']['name'],'',randomKey(3));
          
          if(fileExist($filepost,'lib')){ unlink($uploaddir.$filepost); } // unlink file if exist

          $uploadfile = $uploaddir.$filename;
              
          if( move_uploaded_file($_FILES['file_update']['tmp_name'], $uploadfile) )
          {
            $filepost = $filename;
          }
        }

        $query = "INSERT INTO mst_update_member 
                  (iduser,filecsv,type,status,infosubmitter,createdby,createddate,updatedby,updateddate,ipaddress) 
                  VALUES 
                  ('$adminId','$filepost','$type','0','$desc','$adminId','$datepost','$adminId','$datepost','$ipaddresspost')";
        if(mysqlQuery($query)){
          $status = "1";
          
          $id = "0";
          $query2 = "SELECT MAX(id) AS id 
                     FROM mst_update_member 
                     WHERE status = '0' AND filecsv = '$filepost' AND iduser = '$adminId'";
          $result2 = mysqlQuery($query2);
          if(mysqlNumRows($result2)){
            $row2 = mysqlFetchArray($result2);
            $id = trim($row2['id']);
          }mysqlFreeResult($result2);

          historyActivity($adminId, 3, $id, $datepost, $ipaddresspost); // upload
          notifications($adminId, 1, 0, 2,$id,$datepost,$ipaddresspost);

          $status = "1";
          $successmessage = "Bulk Member berhasil di update";
        }else{
          $status = "2";
          $errormessage = "Maaf, Bulk Member anda tidak berhasil diupdate";
          //echo "Error, insert query failed : $query";
        }
      }
    ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">

        <?php include('_nav.php'); ?>
        
        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mt_40">
            <?php if($status=="1"){ ?>
              <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $successmessage; ?></strong>
              </div>
            <?php } ?>
            <?php if($status=="2"){ ?>
              <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $errormessage; ?></strong>
              </div>
            <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Bulk Update Member
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mb_40">
              <div class="bg_white">
                <div class="row clearfix c_blue">
                  <div class="col-xs-10 col-xs-offset-1 mt_40 mb_40 fs_14">
                    <div class="row clearfix">
                      <form method="post" action="<?php echo siteUrl(); ?>submitter/bulk_update.php" enctype="multipart/form-data">
                        <div class="col-xs-12">
                          <div class="form-group">
                            <label for="keterangan_update" class="w_normal">Keterangan</label>
                            <textarea class="form-control br_0 textarea_input c_grey" id="keterangan_update" name="keterangan_update"></textarea>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <label for="file_update" class="w_normal">Import File</label>
                            <div id="wrap_file_update" class="relative over_hd w100">
                              <span class="bl_1_grey br_1_grey bt_1_grey bb_1_grey dib pl_20 pr_20 c_grey fs_14">Pilih File</span>
                              <span class="dib c_grey fs_14 name_file">Belum pilih file</span>
                              <input type="file" id="file_update" class="c_grey input_file w100" name="file_update" required>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                          <!-- <div id="wrap_check_decrypt" class="checkbox c_grey mt_30">
                            <label>
                              <input type="checkbox" id="decrypt_update" name="decrypt_update" required> Decrypt File
                            </label>
                          </div> -->
                        </div>
                        <div class="col-xs-12 col-sm-3">
                          <div id="wrap_button_update" class="right mt_20"><button type="submit" id="button_update" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Lanjut</button></div>
                          <input type="hidden" name="action" value="update" />
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <?php include( PLUGPATH ."_footer.php"); ?>

      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
</html>
<?php ob_flush(); ?>