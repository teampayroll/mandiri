<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "historyauth";
    $subpage = "";
    $page_name = "History Otorisasi";
?>
<!DOCTYPE html>
<html lang="en">
  <head>

    <?php include( PLUGPATH ."_head.php"); ?>
    <?php
      $pnum=1;
      if(isset($_GET['pnum']))
      {
        $pnum = securefield($_GET['pnum'],5);
      }


      $statusGet ="";
      $nameGet ="";
      $dateStartGet ="";
      $dateEndGet = "";

      $sqlFilterSchedule = 1;
      $sqlFilterUpdate = 1;
      if(isset($_GET['action_activity'])){
        //ACTIVITY
        if($_GET['action_activity'] != "" && is_numeric($_GET['action_activity'])){
          $statusGet = $_GET['action_activity'];
          $sqlFilterSchedule .= " AND mst_schedule.status = $statusGet";
          $sqlFilterUpdate .= " AND mst_update_member.status = $statusGet";
        }
      }

      //Date
       if(isset($_GET['dari_otorisasi']) && isset($_GET['sampai_otorisasi'])){
        if($_GET['dari_otorisasi'] != ""){
          
          $dateStartGet =  $_GET['dari_otorisasi'] ;
          $dateEndGet = $_GET['sampai_otorisasi'] ;

          $dateStart = substr($dateStartGet,6,4) . "/" . substr($dateStartGet,3,2) . "/" . substr($dateStartGet,0,2);
          $dateEnd = substr($dateEndGet,6,4) . "/" . substr($dateEndGet,3,2) . "/" . substr($dateEndGet,0,2);
         
          $sqlFilterSchedule .=  " AND mst_schedule.updateddate BETWEEN '$dateStart' AND DATE_ADD('$dateEnd', INTERVAL 1 DAY)";
          $sqlFilterUpdate .=" AND mst_update_member.updateddate BETWEEN '$dateStart' AND  DATE_ADD('$dateEnd', INTERVAL 1 DAY)";
        }
      }
      if(isset($_GET['user_otorisasi'])){
       if($_GET['user_otorisasi'] != ""){
          $nameGet = $_GET['user_otorisasi'];
          $sqlFilterUpdate .= " AND mst_user.name LIKE '%$nameGet%'";
          $sqlFilterSchedule .= " AND mst_user.name LIKE '%$nameGet%'";
       }
      }

     // set default pagination
      $recordperpages = 1;
      $targetpage = "history_otorisasi.php?action_activity=$statusGet&user_otorisasi=$nameGet&dari_otorisasi=$dateStartGet&sampai_otorisasi=$dateEndGet";
      $startpages = ($pnum-1)*$recordperpages;

      $query = "SELECT  mst_schedule.id, filecsv, mst_user.name as name, mst_schedule.status, infosubmitter, total, -1 as isbulk, mst_schedule.updateddate 
                FROM mst_schedule INNER JOIN mst_user on mst_schedule.iduser = mst_user.id 
                WHERE $sqlFilterSchedule
                UNION 
                SELECT  mst_update_member.id,filecsv, mst_user.name as name, mst_update_member.status, infosubmitter, 1 as total, 1 as isbulk, mst_update_member.updateddate 
                FROM mst_update_member INNER JOIN mst_user on mst_update_member.iduser = mst_user.id 
                 WHERE $sqlFilterUpdate
                ORDER By updateddate DESC";

      $result = mysqlQuery($query);
      $totalrecords = mysqlNumRows($result);
      $query = $query." LIMIT $startpages,$recordperpages";

     
    ?>


  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        <?php include('_nav.php'); ?>
        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                History Otorisasi
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
                <form>
                  <div class="row clearfix">
                    <div class="col-xs-12 mt_40">
                      <div class="row clearfix">
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <label for="action_otorisasi" class="w_normal">Aktifitas</label>
                            <select class="form-control br_0 c_grey" id="action_activity" name="action_activity">
                              <option <?php if($statusGet == ""){ echo 'selected';}?> value="">Semua</option>
                              <option <?php if($statusGet == "0"){ echo 'selected';}?> value="0">Pending Approval</option>
                              <option <?php if($statusGet == "3"){ echo 'selected';}?> value="3">Approve</option>
                               <option <?php if($statusGet == "4"){ echo 'selected';}?> value="4">Reject</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <label for="user_activity" class="w_normal">Nama User</label>
                            <input type="text" class="form-control br_0 c_grey" id="user_otorisasi" name="user_otorisasi" placeholder="" value="<?php echo $nameGet?>">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                          <label for="dari_otorisasi" class="w_normal">Dari Tanggal</label>
                          <div class='input-group date' id='datetimepicker1'>
                              <input type='text' class="form-control br_0 c_grey bg_white cs_pt" id="dari_otorisasi" name="dari_otorisasi" value="<?php echo $dateStartGet?>" readonly="readonly" />
                              <span id="wrap_icon_date" class="input-group-addon cs_df">
                                  <img src="<?php echo siteUrl(); ?>images/icon_date.png" />
                                  <span class="italic c_yellow fs_12">(dd/mm/yyyy)</span>
                              </span>
                          </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <div class="form-group">
                          <label for="sampai_otorisasi" class="w_normal">Sampai Tanggal</label>
                          <div class='input-group date' id='datetimepicker1'>
                              <input type='text' class="form-control br_0 c_grey bg_white cs_pt" id="sampai_otorisasi" name="sampai_otorisasi" value="<?php echo $dateEndGet?>" readonly="readonly" />
                              <span id="wrap_icon_date" class="input-group-addon cs_df">
                                  <img src="<?php echo siteUrl(); ?>images/icon_date.png" />
                                  <span class="italic c_yellow fs_12">(dd/mm/yyyy)</span>
                              </span>
                          </div>
                      </div>
                    </div>
                    <div class="col-xs-12 mb_40">
                      <div class="right">
                        <button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Cari</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>

            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Hasil Pencarian
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mb_40">
              <div class="bg_white c_blue fs_14">
                <div class="table-responsive">
                  <table id="wrap_table" class="table">
                    <thead>
                      <tr>
                        <th>Tanggal</th>
                        <th>Nama User</th>
                        <th>Jumlah Total</th>
                        <th>Type</th>
                        <th>Aktifitas</th>
                        <th>Imported File</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                       <?php 
                          $result = mysqlQuery($query);
                          if(mysqlNumRows($result)){
                            while($row = mysqlFetchArray($result)){
                              $id = trim($row['id']); 
                              $filecsv       = trim($row['filecsv']);
                              $name     = trim($row['name']);
                              $status    = trim($row['status']);
                              $total    = trim($row['total']);
                              $updateddate     = numDate(trim($row['updateddate']));
                              $isbulk  = trim($row['isbulk']); // to verify whether bulk's row or one to many's row
                        ?>
                      <tr>
                        <td><?php echo  $updateddate ?></td>
                        <td><?php echo $name?></td>
                        <td>
                          <?php 
                            if($isbulk == -1){
                              //one to many's row
                              echo "Rp ". number_format($total);
                            }else{
                            //bulk's row, print nothing
                            }
                          ?>
                        </td>
                        <td>
                          <?php
                            if($isbulk == 1){
                              echo 'Bulk Member';
                            }else{
                              echo 'One to Many';
                            }
                          ?>
                        </td>
                        <td>
                          <?php
                            switch($status){
                              case 0:
                                echo 'Pending Approval';
                                break;

                              case 3:
                                echo 'Approve';
                                break;

                              case 4:
                                echo 'Reject';
                                break;

                            

                            }
                          ?>
                        </td>
                        <td><?php echo $filecsv ?></td>
                        <td class="action">
                          <?php if($isbulk == 1){//is bulk update ?> 
                            <a href="detail_bulk.php?id=<?php echo $id ?>"><img src="<?php echo siteUrl(); ?>images/icon_edit.png" /></a>
                          <?php }else { // is one to many?>
                             <a href="detail_one_to_many.php?id=<?php echo $id ?>"><img src="<?php echo siteUrl(); ?>images/icon_edit.png" /></a>
                             <?php }?>

                        </td>
                      </tr>
                      <?php
                          }
                        }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- End of col xs-12 -->
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mb_40">
              <div id="wrap_page" class="right cs_df">
                <?php
                  echo generatePagination($totalrecords, $recordperpages, $targetpage, $pnum, "pnum", 2);
                ?>
              </div>
            </div>
          </div>
        </div>

        <?php include( PLUGPATH ."_footer.php"); ?>
        
      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>

  </body>
</html>
<?php ob_flush(); ?>