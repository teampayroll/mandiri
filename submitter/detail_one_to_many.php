<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "historyauth";
    $subpage = "detail";
    $page_name = "Detail OneToMany";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>
    <?php
      if(isset($_GET['id'])){
        $id = $_GET['id'];
        $query = "SELECT a.createddate, a.noreftransaction, a.jenistransaction, a.total, a.tanggalpembayaran, a.totalrecord, a.filecsv, a.status, a.infosubmitter, b.name  
                 FROM mst_schedule a INNER JOIN mst_user b ON a.iduser = b.id AND b.status = '0' 
                 WHERE a.id = '$id'";
        $result = mysqlQuery($query);
        if(mysqlNumRows($result)){
          $row = mysqlFetchArray($result);
          $waktu_transaksi = numDate(trim($row['createddate']),9);
          $noref_transaksi = trim($row['noreftransaction']);
          $dari_transaksi = trim($row['name']);
          $jenis_transaksi = trim($row['jenistransaction']);
          $jumlah_transaksi = trim($row['total']);
          $tanggal_transaksi = numDate(trim($row['createddate']),0);
          $record_transaksi = trim($row['totalrecord']);
          $file_transaksi = trim($row['filecsv']);
          $status_transaksi = trim($row['status']);
          if($status_transaksi=="0"){
            $status_transaksi = "Pending Approval";
          }else if($status_transaksi=="1"){
            $status_transaksi = "Approve";
          }else if($status_transaksi=="1"){
            $status_transaksi = "Reject";
          }

          $keterangan_transaksi = trim($row['infosubmitter']);
        }else{
          header('Location: history_otorisasi.php');
        }
      }else{
        header('Location: history_otorisasi.php');
      }
    ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">      
        <?php include('_nav.php'); ?>
        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Rincian Transaksi
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mb_40">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
             
                  <div class="row clearfix">
                    <div class="col-xs-12 mt_40">
                      <div class="form-group">
                        <label for="waktu_transaksi" class="w_normal">Waktu Transaksi</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="waktu_transaksi" name="waktu_transaksi" readonly="readonly" value="<?php echo $waktu_transaksi ?>">
                      </div>
                      <div class="form-group">
                        <label for="noref_transaksi" class="w_normal">No Ref Transaksi</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="noref_transaksi" name="noref_transaksi" readonly="readonly" value="<?php echo $noref_transaksi ?>">
                      </div>
                      <div class="form-group">
                        <label for="dari_transaksi" class="w_normal">Dari</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="dari_transaksi" name="dari_transaksi" readonly="readonly" value="<?php echo $dari_transaksi ?>">
                      </div>
                      <div class="form-group">
                        <label for="jenis_transaksi" class="w_normal">Jenis Transaksi</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="jenis_transaksi" name="jenis_transaksi" readonly="readonly" value="<?php echo $jenis_transaksi ?>">
                      </div>
                      <div class="form-group">
                        <label for="jumlah_transaksi" class="w_normal">Jumlah Total</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="jumlah_transaksi" name="jumlah_transaksi" readonly="readonly" value="<?php echo 'Rp '. number_format($jumlah_transaksi) ?>">
                      </div>
                      <div class="form-group">
                        <label for="tanggal_transaksi" class="w_normal">Tanggal Pembayaran</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="tanggal_transaksi" name="tanggal_transaksi" readonly="readonly" value="<?php echo $tanggal_transaksi ?>">
                      </div>
                      <div class="form-group">
                        <label for="record_transaksi" class="w_normal">Jumlah Record</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="record_transaksi" name="record_transaksi" readonly="readonly" value="<?php echo $record_transaksi ?>">
                      </div>
                      <div class="form-group">
                        <label for="file_transaksi" class="w_normal">Imported File</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="file_transaksi" name="file_transaksi" readonly="readonly" value="<?php echo $file_transaksi ?>">
                      </div>
                      <div class="form-group">
                        <label for="status_transaksi" class="w_normal">Status</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="status_transaksi" name="status_transaksi" readonly="readonly" value="<?php echo $status_transaksi ?>">
                      </div>
                      <div class="form-group">
                        <label for="keterangan_transaksi" class="w_normal">Keterangan</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="keterangan_transaksi" name="keterangan_transaksi" readonly="readonly" value="<?php echo $keterangan_transaksi ?>">
                      </div>
                    </div>
                    <div class="col-xs-12 mb_40">
                      <div class="right">
                        <a href="history_otorisasi.php"><button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Kembali</button></a>
                      </div>
                    </div>
                  </div>
                
              </div>
            </div>
          </div>
        </div>

        <?php include( PLUGPATH ."_footer.php"); ?>

      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
  </body>
</html>
<?php ob_flush(); ?>