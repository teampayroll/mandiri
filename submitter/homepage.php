<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "homepage";
    $subpage = "homepage";
    $page_name = "Homepage";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>

    <link href="<?php echo siteUrl(); ?>css/homepage.css" rel="stylesheet">
  </head>


  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        <?php include('_nav.php'); ?>

        <div class="col-xs-12 body">
          <div class="col-sm-offset-6 col-sm-6">
            Selamat Datang <br>
            <span> Di layanan Mandiri E-Cash
          </div>
        </div>
        <footer>
          <?php include( PLUGPATH ."_footer.php"); ?>
        </footer>
      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
</html>
<?php ob_flush(); ?>

<script>

</script>