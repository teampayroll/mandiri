<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "schedule";
    $subpage = "schedule";
    $page_name = "One to Many Transfer";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>

    <?php
      if( postExist('action') && equalPostStr('action','schedule') ){
        $jumlah_schedule = secureText($_POST["jumlah_schedule"]);
        $jenis_schedule = secureText($_POST["jenis_schedule"]);
        $record_schedule = secureText($_POST["record_schedule"]);
        $tanggal_schedule_get = secureText($_POST["tanggal_schedule"]);
          /*numDate(secureText($_POST["tanggal_schedule"]),5);*/
        $tanggal_schedule =  substr($tanggal_schedule_get,6,4). "-". substr($tanggal_schedule_get,3,2). "-" . substr($tanggal_schedule_get,0,2).",00:00:00";
       
        $keterangan_schedule = secureText($_POST["keterangan_schedule"]);

        $type = "0";

        $uploaddir = fileUpload('lib');

        $filepost = "";
        if( !equalStr($_FILES['file_schedule']['name'],'') )
        {
          $filename = secureFile($_FILES['file_schedule']['name'],'',randomKey(3));
          
          if(fileExist($filepost,'lib')){ unlink($uploaddir.$filepost); } // unlink file if exist

          $uploadfile = $uploaddir.$filename;
              
          if( move_uploaded_file($_FILES['file_schedule']['tmp_name'], $uploadfile) )
          {
            $filepost = $filename;
          }
        }

        $noreftransaction = rand(100, 999).date("Y").date("m").date("d").date("H").date("i").date("s");

        $query = "INSERT INTO mst_schedule 
                  (iduser,noreftransaction,total,jenistransaction,totalrecord,tanggalpembayaran,filecsv,infosubmitter,type,status,createdby,createddate,updatedby,updateddate,ipaddress) 
                  VALUES 
                  ('$adminId','$noreftransaction','$jumlah_schedule','$jenis_schedule','$record_schedule','$tanggal_schedule','$filepost','$keterangan_schedule','$type','1','$adminId','$datepost','$adminId','$datepost','$ipaddresspost')";
        if(mysqlQuery($query)){
          $status = "1";
          
          $id = "0";
          $query2 = "SELECT MAX(id) AS id 
                     FROM mst_schedule 
                     WHERE status = '1' AND filecsv = '$filepost' AND iduser = '$adminId'";
          $result2 = mysqlQuery($query2);
          if(mysqlNumRows($result2)){
            $row2 = mysqlFetchArray($result2);
            $id = trim($row2['id']);
          }mysqlFreeResult($result2);

           
          redirectSite('submitter/preview.php?id='.$id);

        }else{
          $status = "2";
          $errormessage = "Maaf, One to Many Transfer anda tidak berhasil dibuat.";
          //echo "Error, insert query failed : $query";
        }
      }

      if( postExist('action') && equalPostStr('action','active') ){
        $id = trim($_POST['id']);

        $query = "UPDATE mst_schedule SET status = '0', updatedby = '$adminId', updateddate = '$datepost', ipaddress = '$ipaddresspost' WHERE id = '$id'";
        if(mysqlQuery($query)){
          $status = "1";
          $successmessage = "One to Many Transfer anda berhasil dibuat.";

          notifications($adminId, 0, 0, 2,$id,$datepost,$ipaddresspost);// notif schedule
          historyActivity($adminId, 2, $id, $datepost, $ipaddresspost); // schedule
        }else{
          $status = "2";
          $errormessage = "Maaf, One to Many Transfer anda tidak berhasil dibuat.";
          //echo "Error, insert query failed : $query";
        }
      }
    ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        
        <?php include('_nav.php'); ?>
        
        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mt_40">
            <?php if($status=="1"){ ?>
              <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $successmessage; ?></strong>
              </div>
            <?php } ?>
            <?php if($status=="2"){ ?>
              <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $errormessage; ?></strong>
              </div>
            <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                One to Many Transfer
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 mb_40">
              <div class="bg_white">
                <div class="row clearfix c_blue">
                  <div class="col-xs-10 col-xs-offset-1 mt_40 mb_40 fs_14">
                    <div class="row clearfix">
                      <form method="post" action="<?php echo siteUrl(); ?>submitter/schedule.php" enctype="multipart/form-data">
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <label for="jumlah_schedule" class="w_normal">Jumlah</label>
                            <input type="text" class="form-control br_0 c_grey" id="jumlah_schedule" name="jumlah_schedule" value="0" required>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <label for="jenis_schedule" class="w_normal">Jenis Transaksi</label>
                            <input type="text" class="form-control br_0 c_grey" id="jenis_schedule" name="jenis_schedule" required>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <label for="record_schedule" class="w_normal">Jumlah Record</label>
                            <input type="text" class="form-control br_0 c_grey" id="record_schedule" name="record_schedule" value="0" required>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                              <label for="tanggal_schedule" class="w_normal">Tanggal Pembayaran</label>
                              <div class='input-group date' id='datetimepicker1'>
                                  <input type='text' class="form-control br_0 c_grey bg_white cs_pt" id="tanggal_schedule" name="tanggal_schedule" readonly="readonly" value="<?php echo $dateschedule; ?>" required/>
                                  <span id="wrap_icon_date" class="input-group-addon cs_df">
                                      <img src="<?php echo siteUrl(); ?>images/icon_date.png" />
                                      <span class="italic c_yellow fs_12">(dd/mm/yyyy)</span>
                                  </span>
                              </div>
                          </div>
                        </div>
                        <div class="col-xs-12">
                          <div class="form-group">
                            <label for="keterangan_schedule" class="w_normal">Keterangan</label>
                            <textarea class="form-control br_0 textarea_input c_grey" id="keterangan_schedule" name="keterangan_schedule"></textarea>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                          <div class="form-group">
                            <label for="file_schedule" class="w_normal">Import File</label>
                            <div id="wrap_file_update" class="relative over_hd w100">
                              <span class="bl_1_grey br_1_grey bt_1_grey bb_1_grey dib pl_20 pr_20 c_grey fs_14">Pilih File</span>
                              <span class="dib c_grey fs_14 name_file">Belum pilih file</span>
                              <input type="file" id="file_schedule" class="c_grey input_file w100" name="file_schedule" required>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                          <!-- <div id="wrap_check_decrypt" class="checkbox mt_30 c_grey">
                            <label>
                              <input type="checkbox" id="decrypt_schedule" name="decrypt_schedule"> Decrypt File
                            </label>
                          </div> -->
                        </div>
                        <div class="col-xs-12 col-sm-3">
                          <div id="wrap_button_update" class="right mt_20"><button type="submit" id="button_schedule" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Lanjut</button></div>
                          <input type="hidden" name="action" value="schedule" />
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <?php include( PLUGPATH ."_footer.php"); ?>

      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
</html>
<?php ob_flush(); ?>