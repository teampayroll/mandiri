<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "historyauth";
    $subpage = "detail";
    $page_name = "Detail OneToMany";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>
    <?php
      if(isset($_GET['id'])){
        $id = $_GET['id'];
        $query = "SELECT a.createddate, b.name, a.filecsv, a.commentapprover_a, a.status, a.infosubmitter, b.name  
                 FROM mst_update_member a INNER JOIN mst_user b ON a.iduser = b.id AND b.status = '0' 
                 WHERE a.id = '$id'";

        $result = mysqlQuery($query);
        if(mysqlNumRows($result)){
          
          $row = mysqlFetchArray($result);
          $waktu = numDate(trim($row['createddate']),9);
          $dari = trim($row['name']);
          $filename = trim($row['filecsv']);
          $status = trim($row['status']);
          if($status=="0"){
            $status = "Pending Approval";
          }else if($status=="3"){
            $status = "Approve";
          }else if($status=="4"){
            $status = "Reject";
          }
          $keterangan_submiter= trim($row['infosubmitter']);
          $keterangan_approver= trim($row['commentapprover_a']);
        }else{
           header('Location: history_otorisasi.php');
        }
      }else{
        header('Location: history_otorisasi.php');
      }
    ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">      
        <?php include('_nav.php'); ?>
        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Rincian Bulk Member
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mb_40">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
                  <div class="row clearfix">
                    <div class="col-xs-12 mt_40">
                      <div class="form-group">
                        <label for="waktu_transaksi" class="w_normal">Waktu Update</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="waktu_transaksi" name="waktu_transaksi" readonly="readonly" value="<?php echo $waktu ?>">
                      </div>
                      <div class="form-group">
                        <label for="dari_transaksi" class="w_normal">Dari</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="dari_transaksi" name="dari_transaksi" readonly="readonly" value="<?php echo $dari?>">
                      </div>
     
                      <div class="form-group">
                        <label for="file_transaksi" class="w_normal">Imported File</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="file_transaksi" name="file_transaksi" readonly="readonly" value="<?php echo $filename ?>">
                      </div>
                      <div class="form-group">
                        <label for="status_transaksi" class="w_normal">Status</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="status_transaksi" name="status_transaksi" readonly="readonly" value="<?php echo $status ?>">
                      </div>
                      <div class="form-group">
                        <label for="keterangan_transaksi" class="w_normal">Keterangan Submitter</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="keterangan_transaksi" name="keterangan_transaksi" readonly="readonly" value="<?php echo $keterangan_submiter ?>">
                      </div>
                       <div class="form-group">
                        <label for="keterangan_transaksi" class="w_normal">Keterangan Approver</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="keterangan_transaksi" name="keterangan_transaksi" readonly="readonly" value="<?php echo $keterangan_approver ?>">
                      </div>
                    </div>
                    <div class="col-xs-12 mb_40">
                      <div class="right">
                        <a href="history_otorisasi.php"><button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Kembali</button></a>
                      </div>
                    </div>
                  </div>
                
              </div>
            </div>
          </div>
        </div>

        <?php include( PLUGPATH ."_footer.php"); ?>

      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
  </body>
</html>
<?php ob_flush(); ?>