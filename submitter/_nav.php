<div id="wrap_nav" class="col-xs-12 bg_white bb_1_grey">
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><img src="<?php echo siteUrl(); ?>images/logo.png" class="img-responsive"></a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="nav">
        <ul class="nav navbar-nav navbar-right up">
          <li <?php if($page=="homepage"){ ?>class="active"<?php } ?>><a href="<?php echo siteUrl(); ?>submitter/homepage.php">Homepage</a></li>
          <li <?php if($page=="bulkupdate"){ ?>class="active"<?php } ?>><a href="<?php echo siteUrl(); ?>submitter/bulk_update.php">Bulk Member</a></li>
          <li <?php if($page=="schedule"){ ?>class="active"<?php } ?>><a href="<?php echo siteUrl(); ?>submitter/schedule.php">One To Many</a></li>          
          <li <?php if($page=="historyactivity"){ ?>class="active"<?php } ?> ><a href="<?php echo siteUrl(); ?>submitter/history_activity.php">History Activity</a></li>
          <li <?php if($page=="historyauth"){ ?>class="active"<?php } ?>><a href="<?php echo siteUrl(); ?>submitter/history_otorisasi.php">History Authorization</a></li>
          <li class="dropdown <?php if($page=="profile"){ ?>active<?php } ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $adminName; ?> <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li class="bg_yellow"><a href="<?php echo siteUrl(); ?>submitter/profile.php">Edit Profile</a></li>
              <li class="bg_white"><a href="<?php echo siteUrl(); ?>logout.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
</div>