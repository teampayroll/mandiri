<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "profile";
    $subpage = "edit";
    $page_name = "Edit Profile";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>

    <?php
      $name = "";
      $email = "";
      $phone = "";
      $password = "";

      $query = "SELECT name, email, phone, password FROM mst_user WHERE id = '$adminId'";
      $result = mysqlQuery($query);
      if(mysqlNumRows($result)){
        $row = mysqlFetchArray($result);
        $name = trim($row['name']);
        $email = trim($row['email']);
        $phone = trim($row['phone']);
        $password = trim(decryptIt($row['password']));
      }mysqlFreeResult($result);

      if( postExist('action') && equalPostStr('action','update') ){
        $name = secureLimit($_POST["name"],255);
        $email = secureLimit($_POST["email"],255);
        $phone = trim($_POST["phone"]);
        $old_password = secureLimit($_POST["old_password"],255);
        $new_password = secureLimit($_POST["new_password"],255);

        $query2 = "SELECT id FROM mst_user WHERE email = '$email' AND id != '$adminId' AND (status = '0' || status = '1')";
        $result2 = mysqlQuery($query2);
        if(!mysqlNumRows($result2)){
          if($old_password=="" && $new_password==""){
            $query = "UPDATE mst_user SET name = '$name', email = '$email', phone = '$phone', updatedby = '$adminId', updateddate = '$datepost', ipaddress = '$ipaddresspost' WHERE id = '$adminId'";
            if(mysqlQuery($query)){
              $status = "1";
              $successmessage = "Edit profile anda berhasil.";

              $_SESSION['admin'] = $name;
              $_SESSION['email'] = $email;

              //this adds 30 days to the current time
              // 30 days (2592000 = 60 seconds * 60 mins * 24 hours * 30 days) 
              // 1 hours (3600 = 60 seconds * 60 mins * 1 hours)
              $days = 3600 + time();
              setcookie('admin', $name, $days);
              setcookie('email', $email, $days);
            }else{
              $status = "2";
              $errormessage = "Maaf, Edit profile anda tidak berhasil.";
            }
          }else{
            if($old_password==""){
              $status = "2";
              $errormessage = "Maaf, Password lama harus di isi.";
            }else if($new_password==""){
              $status = "2";
              $errormessage = "Maaf, Password baru harus di isi.";
            }else if($old_password==$password){
              $new_password = encryptIt($new_password);
              $query = "UPDATE mst_user SET name = '$name', email = '$email', phone = '$phone', password = '$new_password', updatedby = '$adminId', updateddate = '$datepost', ipaddress = '$ipaddresspost' WHERE id = '$adminId'";
              if(mysqlQuery($query)){
                $status = "1";
                $successmessage = "Edit profile anda berhasil.";

                $_SESSION['admin'] = $name;
                $_SESSION['email'] = $email;

                //this adds 30 days to the current time
                // 30 days (2592000 = 60 seconds * 60 mins * 24 hours * 30 days) 
                // 1 hours (3600 = 60 seconds * 60 mins * 1 hours)
                $days = 3600 + time();
                setcookie('admin', $name, $days);
                setcookie('email', $email, $days);
              }else{
                $status = "2";
                $errormessage = "Maaf, Edit profile anda tidak berhasil.";
              }
            }else{
              $status = "2";
              $errormessage = "Maaf, Password lama anda tidak sesuai.";
            }
          }
        }else{
          $status = "2";
          $errormessage = "Maaf, Email yang akan anda gunakan sudah tersimpan pada sistem.";
        }mysqlFreeResult($result2);

      }
    ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        
        <?php include('_nav.php'); ?>

        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mt_40">
            <?php if($status=="1"){ ?>
              <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $successmessage; ?></strong>
              </div>
            <?php } ?>
            <?php if($status=="2"){ ?>
              <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $errormessage; ?></strong>
              </div>
            <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Edit Profile
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mb_40">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
                <form action="<?php echo siteUrl(); ?>submitter/profile_edit.php" method="post">
                  <div class="row clearfix">
                    <div class="col-xs-12 mt_40">
                      <div class="form-group">
                        <label for="name" class="w_normal">Name</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="name" name="name" value="<?php echo $name; ?>" required>
                      </div>
                      <div class="form-group">
                        <label for="email" class="w_normal">Email</label>
                        <input type="email" class="form-control br_0 bg_white c_grey" id="email" name="email" value="<?php echo $email; ?>" required>
                      </div>
                      <div class="form-group">
                        <label for="old_password" class="w_normal">Password Lama</label>
                        <input type="password" class="form-control br_0 bg_white c_grey" id="old_password" name="old_password" value="">
                      </div>
                      <div class="form-group">
                        <label for="new_password" class="w_normal">Password Baru</label>
                        <input type="password" class="form-control br_0 bg_white c_grey" id="new_password" name="new_password" value="">
                      </div>
                      <div class="form-group">
                        <label for="phone" class="w_normal">Phone</label>
                        <input type="text" class="form-control br_0 bg_white c_grey" id="phone" name="phone" value="<?php echo $phone; ?>" required>
                      </div>
                    </div>
                    <div class="col-xs-12 mb_40">
                      <div class="right">
                        <a href="<?php echo siteUrl(); ?>submitter/profile.php">
                          <button type="button" id="button_cancel" class="btn btn-default button_white fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Batalkan</button>
                        </a>
                        <button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Simpan</button>
                        <input type="hidden" name="action" value="update" />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        
        <?php include( PLUGPATH ."_footer.php"); ?>
        
      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
</html>
<?php ob_flush(); ?>