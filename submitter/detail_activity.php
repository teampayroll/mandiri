<?php 
    if (!isset($_SESSION)) { session_start(); }
    ob_start();

    include("include.php");

    $page = "historyactivity";
    $subpage = "detail";
    $page_name = "History Activity";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include( PLUGPATH ."_head.php"); ?>

    <?php

      if(!isset($_GET['id'])){
        header('Location: history_activity.php');
      }

      $id = $_GET['id'];

      $query = "SELECT a.id, b.name, a.type, a.idrelated, a.updateddate , 
      c.filecsv as csvschedule, d.filecsv as csvbulk, c.infosubmitter as commentschedule, d.infosubmitter as commentbulk,
      
      c.idapprover_a as idapproveraschedule, c.commentapprover_a as commentapproveraschedule,
      c.idapprover_b as idapproverbschedule, c.commentapprover_b as commentapproverbschedule,

      d.idapprover_a as idapproverabulk, d.commentapprover_a as commentapproverabulk,
      d.idapprover_b as idapproverbbulk, d.commentapprover_b as commentapproverbbulk
      
      FROM mst_history_activity a 
      LEFT JOIN mst_user b ON a.iduser = b.id
      LEFT JOIN mst_schedule c ON  a.idrelated = c.id 
      LEFT JOIN mst_update_member d ON  a.idrelated = d.id 
      WHERE a.id = $id
      ORDER BY a.id desc";

      $result = mysqlQuery($query);
      if(mysqlNumRows($result)){
        while($row = mysqlFetchArray($result)){
          $id = trim($row['id']); 
          $name = trim($row['name']);
          $idrelated =  trim($row['idrelated']);
          $updateddate     = numDate(trim($row['updateddate']));
          $type = trim($row['type']); 
          switch ($type) {
            case '0':
              $activity = "Login";
              break;
            case '1':
              $activity = "Logout";
              break;
            case '2':
              $activity  = "One to Many";
              break;
            case '3': 
              $activity = "Bulk Member";
              break;
            case '4':
              $activity = "Approval";
              break;
           case '5':
              $activity = "Approval";
              break;
           
          }


          if($type == 2){ // if schedule
             $filecsv = trim($row['csvschedule']); 
             $comment = trim($row['commentschedule']); 
          }else if($type == 3){ // if bulk member
             $filecsv = trim($row['csvbulk']);
             $comment = trim($row['commentbulk']); 
          }else if($type == 4){ // if schedule approvar
             $filecsv = trim($row['csvschedule']); 
             $idapproveraschedule = $row['idapproveraschedule'];
             if( $idapproveraschedule != 0){ // approver a
              $comment = trim($row['commentapproveraschedule']); 
              }else{  //approver b
              $comment = trim($row['commentapproverbschedule']);  
              }
          }else if($type == 5){ // if bulk approval
             $filecsv = trim($row['csvbulk']);
             $idapproverabulk = $row['idapproverabulk'];
             if( $idapproverabulk != 0){ // approver a
              $comment = trim($row['commentapproverabulk']); 
              }else{ //approver b
              $comment = trim($row['commentapproverbbulk']);  
              }
             
          }else{
            $filecsv = "";
            $comment = "";
          }
        }
      }else{
        header('Location: history_activity.php');
      }

    ?>

  </head>
  <body class="cs_df arial">
    <div class="container-fluid">
      <div class="row clearfix">
        
        <?php include('_nav.php'); ?>

        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mt_40">
              <div class="bg_blue2 fs_14 bold c_white pt_15 pb_15 pl_20 pr_20">
                Rincian Transaksi
              </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mb_40">
              <div class="bg_white pl_20 pr_20 c_blue fs_14">
                <div class="row clearfix">
                  <div class="col-xs-12 mt_40">
                    <div class="form-group">
                      <label for="waktu_transaksi" class="w_normal">Waktu Activity</label>
                      <input type="text" class="form-control br_0 bg_white c_grey" id="waktu_transaksi" name="waktu_transaksi" readonly="readonly" value="<?php echo $updateddate ?>">
                    </div>
                    <div class="form-group">
                      <label for="noref_transaksi" class="w_normal">Nama User</label>
                      <input type="text" class="form-control br_0 bg_white c_grey" id="noref_transaksi" name="noref_transaksi" readonly="readonly" value="<?php echo $name ?>">
                    </div>
                    <div class="form-group">
                      <label for="dari_transaksi" class="w_normal">Activity</label>
                      <input type="text" class="form-control br_0 bg_white c_grey" id="dari_transaksi" name="dari_transaksi" readonly="readonly" value="<?php echo $activity ?>">
                    </div>
                    <div class="form-group">
                      <label for="jenis_transaksi" class="w_normal">Comment</label>
                      <input type="text" class="form-control br_0 bg_white c_grey" id="jenis_transaksi" name="jenis_transaksi" readonly="readonly" value="<?php echo $comment ?>">
                    </div>
                    <div class="form-group">
                      <label for="jumlah_transaksi" class="w_normal">Imported File</label>
                      <input type="text" class="form-control br_0 bg_white c_grey" id="jumlah_transaksi" name="jumlah_transaksi" readonly="readonly" value=" <?php echo $filecsv ?>">
                    </div>
                    
                  <div class="col-xs-12 col-sm-12 mt_40">
                    <div class="right">
                          <a href="history_activity.php"><button type="submit" id="button_submit" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Kembali</button></a>
                    </div>                                          
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <?php include( PLUGPATH ."_footer.php"); ?>
        
      </div>
    </div>

    <?php include( PLUGPATH ."_javascript.php"); ?>
    
  </body>
</html>
<?php ob_flush(); ?>