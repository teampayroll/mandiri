<?php
	$days = 3600 + time(); //this adds 1 hour to the current time, 1 hour (3600 = 60 sec * 60 min)
	$past = time() - 10; //this makes the time 10 seconds ago 
	$datenow = date("Y-m-d"); // the date now
	$datepost = dateGMT(); // the date post
	$dateschedule = numDate($datepost,0);
	$ipaddresspost = $_SERVER['REMOTE_ADDR']; // the ipaddress post

	$adminName = "Administrator"; // the default administrator of the name is administrator
	if(sessionExist('admin')){
		$adminName = $_SESSION['admin'];
	}else if(cookieExist('admin')){
		$_SESSION['admin'] = $_COOKIE['admin'];
		$adminName = $_COOKIE['admin'];
	}

	$adminLogin = ""; // the default administrator of the login is null
	if(sessionExist('email')){
		$adminLogin = $_SESSION['email'];
	}else if(cookieExist('email')){
		$_SESSION['email'] = $_COOKIE['email'];
		$adminLogin = $_SESSION['email'];
	}

	$adminId = "0"; // the default administrator of the id is null
	$adminType = "0"; // the default administrator of the type is null
	if(sessionExist('id')){
		$adminId = $_SESSION['id'];

		if(sessionExist('type')){
			$adminType = $_SESSION['type'];
		}
	}else if(cookieExist('id')){
		$_SESSION['id'] = $_COOKIE['id'];
		$adminId = $_SESSION['id'];

		if(cookieExist('type')){
			$_SESSION['type'] = $_COOKIE['type'];
			$adminType = $_SESSION['type'];
		}
	}

	$status = "0"; // the default of status is null
	if(sessionExist('status')){
		$status = $_SESSION['status'];
		$_SESSION['status'] = "0";
	}

	$successmessage = "";
	$errormessage = "";
	if(sessionExist('successmessage')){
		$successmessage = $_SESSION['successmessage'];
		$_SESSION['successmessage'] = "";
	}
	if(sessionExist('errormessage')){
		$errormessage = $_SESSION['errormessage'];
		$_SESSION['errormessage'] = "";
	}

	function historyActivity( $iduser, $type, $idrelated, $date, $ipaddress ){
		// 0: login | 1: logout | 2: one to many / schedule | 3: bulk member | 4: approval
		$query = "INSERT INTO `mst_history_activity`
		(iduser, type, idrelated, status, createdby, createddate, updatedby, updateddate, ipaddress) 
		VALUES (
		$iduser, $type, $idrelated, 0, $iduser, '$date', $iduser, '$date', '$ipaddress')
		";
		mysqlQuery($query);
	}

	function notifications( $iduser, $type_notif, $type_submitter, $type_approver, $idrelated, $date, $ipaddress ){
		// type notif 0: submitter one to many to approver | 1: submitter bulk member to approver | 2: approver to approver
		// type submitter 0: upload schedule | 1: h-2 schedule | 2: h-1 schedule
		// type approver 0: team a | 1: team b | 2: all

		$query = "INSERT INTO `mst_notification`
		(type_notif, type_submitter, type_approver, idrelated, status, createdby, createddate, updatedby, updateddate, ipaddress) 
		VALUES (
		$type_notif, $type_submitter, $type_approver, $idrelated, 0, $iduser, '$date', $iduser, '$date', '$ipaddress')
		";
		
		mysqlQuery($query);
	}

	function generatePagination($totalrecords, $recordperpages, $targetpage, $page, $passname = "page", $adjacents = 2){	
		// find mark (?) on URL
		if (stripos($targetpage, "?")==true) {
			$targetpage = $targetpage."&";
		} else {
			$targetpage = $targetpage."?";
		}

		/* Setup page vars for display. */
		if ($page == 0) $page = 1;					//if no page var is given, default to 1.
		$prev = $page - 1;							//previous page is page - 1
		$next = $page + 1;							//next page is page + 1
		$lastpage = ceil($totalrecords/$recordperpages);		//lastpage is = total pages / items per page, rounded up.
		$lpm1 = $lastpage - 1;						//last page minus 1
		
		/* 
			Now we apply our rules and draw the pagination object. 
			We're actually saving the code to a variable in case we want to draw it more than once.
		*/
		$pagination = "";
		if($lastpage > 1)
		{	
			$pagination .= "<div class=\"pagination\">";
			//previous button
			if ($page > 1) 
				$pagination.= "<a href=\"$targetpage"."$passname=$prev\"><<</a>";
			else
				$pagination.= "<span class=\"disabled\"><<</span>";	
			
			//pages	
			if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage"."$passname=$counter\">$counter</a>";					
				}
			}
			elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if($page < 1 + ($adjacents * 2))		
				{
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.= "<a href=\"$targetpage"."$passname=$counter\">$counter</a>";					
					}
					$pagination.= "<span>...&nbsp</span>";
					$pagination.= "<a href=\"$targetpage"."$passname=$lpm1\">$lpm1</a>";
					$pagination.= "<a href=\"$targetpage"."$passname=$lastpage\">$lastpage</a>";		
				}
				//in middle; hide some front and some back
				elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
				{
					$pagination.= "<a href=\"$targetpage"."$passname=1\">1</a>";
					$pagination.= "<a href=\"$targetpage"."$passname=2\">2</a>";
					$pagination.= "<span>...&nbsp</span>";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.= "<a href=\"$targetpage"."$passname=$counter\">$counter</a>";					
					}
					$pagination.= "<span>...&nbsp</span>";
					$pagination.= "<a href=\"$targetpage"."$passname=$lpm1\">$lpm1</a>";
					$pagination.= "<a href=\"$targetpage"."$passname=$lastpage\">$lastpage</a>";		
				}
				//close to end; only hide early pages
				else
				{
					$pagination.= "<a href=\"$targetpage"."$passname=1\">1</a>";
					$pagination.= "<a href=\"$targetpage"."$passname=2\">2</a>";
					$pagination.= "<span>...&nbsp</span>";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.= "<a href=\"$targetpage"."$passname=$counter\">$counter</a>";					
					}
				}
			}
			
			//next button
			if ($page < $counter - 1) 
				$pagination.= "<a href=\"$targetpage"."$passname=$next\">>></a>";
			else
				$pagination.= "<span class=\"disabled\">>></span>";
			$pagination.= "</div>\n";		
		}
		return $pagination;
	}
?>