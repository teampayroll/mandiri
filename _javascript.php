<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo siteUrl(); ?>js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo siteUrl(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo siteUrl(); ?>js/moment.js"></script>
<script src="<?php echo siteUrl(); ?>js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo siteUrl(); ?>js/parsley.min.js"></script>
<script type="text/javascript">
    $(function () {
    	$(".input_file").change(function() {
          var file = $(this).val();
          var str = file.split(/(\\|\/)/g).pop();
          $(".input_file").prev().html(str);
        });

        $('#dari_activity').datetimepicker({
          useCurrent: true,
          ignoreReadonly: true,
          format: "DD/MM/YYYY"
        });
        $('#sampai_activity').datetimepicker({
          useCurrent: true,
          ignoreReadonly: true,
          format: "DD/MM/YYYY"
        });
        $("#dari_activity").on("dp.change", function (e) {
          $('#sampai_activity').data("DateTimePicker").minDate(e.date);
        });
        $("#sampai_activity").on("dp.change", function (e) {
            $('#dari_activity').data("DateTimePicker").maxDate(e.date);
        });

        $('#dari_otorisasi').datetimepicker({
          useCurrent: true,
          ignoreReadonly: true,
          format: "DD/MM/YYYY"
        });
        $('#sampai_otorisasi').datetimepicker({
          useCurrent: true,
          ignoreReadonly: true,
          format: "DD/MM/YYYY"
        });
        $("#dari_otorisasi").on("dp.change", function (e) {
          $('#sampai_otorisasi').data("DateTimePicker").minDate(e.date);
        });
        $("#sampai_otorisasi").on("dp.change", function (e) {
            $('#dari_otorisasi').data("DateTimePicker").maxDate(e.date);
        });

        $(".input_file").change(function() {
          var file = $(this).val();
          var str = file.split(/(\\|\/)/g).pop();
          $(".input_file").prev().html(str);
        });
        $('#tanggal_schedule').datetimepicker({
          useCurrent: true,
          ignoreReadonly: true,
          format: "DD/MM/YYYY"
        });
        $("#tanggal_schedule").on("dp.show", function (e) {
          $('#tanggal_schedule').data("DateTimePicker").minDate(e.date);
        });

        $('#dari_notif').datetimepicker({
          useCurrent: true,
          ignoreReadonly: true,
          format: "DD/MM/YYYY"
        });
        $('#sampai_notif').datetimepicker({
          useCurrent: true,
          ignoreReadonly: true,
          format: "DD/MM/YYYY"
        });
        $("#dari_notif").on("dp.change", function (e) {
          $('#sampai_notif').data("DateTimePicker").minDate(e.date);
        });
        $("#sampai_notif").on("dp.change", function (e) {
            $('#dari_notif').data("DateTimePicker").maxDate(e.date);
        });

        $("#jumlah_schedule").keyup(function() {
          var jumlah = $("#jumlah_schedule").val();
          if(!jumlah.match(/^[0-9]+$/)){
            $("#jumlah_schedule").val("0");
          }
        });

        $("#record_schedule").keyup(function() {
          var record = $("#record_schedule").val();
          if(!record.match(/^[0-9]+$/)){
            $("#record_schedule").val("0");
          }
        });

        $("#button_cancel").click(function() {
          window.location = "<?php echo siteUrl(); ?>submitter/cancel_schedule.php";
        });

        $("#phone").keyup(function() {
          var phone = $("#phone").val();
          if(!phone.match(/^[0-9]+$/)){
            $("#phone").val("");
          }
        });
    });
</script>