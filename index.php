<?php
    if (!isset($_SESSION)) { session_start(); }
    ob_start();
    
    $page = "login";
    $subpage = "";
    $page_name = "Login";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <?php include('_head.php'); ?>

    <?php
      if( postExist('action') && equalPostStr('action','login') ){
        $login = secureLimit($_POST["id_login"], 255);
        $pass = secureLimit($_POST["pass_login"], 255);

        $query = "SELECT id, name, email, password, type FROM mst_user WHERE email = '$login' AND status = '0'";
        $result = mysqlQuery($query);
        if(mysqlNumRows($result)){
            $row = mysqlFetchArray($result);
            $adminId = trim($row['id']);
            $adminName = trim($row['name']);
            $adminPass = decryptIt(trim($row['password']));
            $adminType = trim($row['type']);
            $adminLogin = $login;

            if(equalStr($pass,$adminPass)){
                $_SESSION['id'] = $adminId;
                $_SESSION['admin'] = $adminName;
                $_SESSION['email'] = $adminLogin;
                $_SESSION['type'] = $adminType;

                //this adds 30 days to the current time
                // 30 days (2592000 = 60 seconds * 60 mins * 24 hours * 30 days) 
                // 1 hours (3600 = 60 seconds * 60 mins * 1 hours)
                $days = 3600 + time();
                setcookie('id', $adminId, $days);
                setcookie('admin', $adminName, $days);
                setcookie('email', $adminLogin, $days);
                setcookie('type', $adminType, $days);
                
                historyActivity($adminId, 0, 0, $datepost, $ipaddresspost);
                if($adminType=="0"){
                  redirectSite('submitter'); // redirect page
                  exit;
                }else if($adminType=="1"){
                  redirectSite('approver'); // redirect page
                  exit;
                }else if($adminType=="2"){
                  redirectSite('superadmin'); // redirect page
                  exit;
                }
            }
            
            $status = "2"; // custom error
            $errormessage = "Invalid Password!";  // invalid password
        }else{
            $status = "2"; // custom error
            $errormessage = "Invalid Email!";  // invalid username    
        }mysqlFreeResult($result);
      }
    ?>

  </head>
  <body class="cs_df arial bg_white">
    <div class="container-fluid">
      <div class="row clearfix">
        <div class="col-xs-12 bg_blue">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mt_90">
            <?php if($status=="1"){ ?>
              <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $successmessage; ?></strong>
              </div>
            <?php } ?>
            <?php if($status=="2"){ ?>
              <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong><?php echo $errormessage; ?></strong>
              </div>
            <?php } ?>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 mb_90">
              <div class="bg_white">
                <div class="row clearfix c_blue">
                  <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 mt_40 mb_40 fs_14">
                    <div class="row clearfix">
                      <div class="col-xs-12 bold">Login</div>
                      <div class="col-xs-12 mt_15 w_normal">Silahkan masukkan ID atau email, dan password Anda untuk masuk ke sistem mandiri e-cash</div>
                      <div class="col-xs-12 mt_30">
                        <form action="<?php echo siteUrl(); ?>" method="post">
                          <div class="form-group">
                            <label for="id_login" class="w_normal">Email</label>
                            <input type="text" class="form-control br_0 c_grey" id="id_login" name="id_login" placeholder="" required>
                          </div>
                          <div class="form-group mt_20">
                            <label for="pass_login" class="w_normal">Password</label>
                            <input type="password" class="form-control br_0 c_grey" id="pass_login" name="pass_login" placeholder="" required>
                          </div>
                          <div class="right mt_20"><button type="submit" id="button_login" class="btn btn-default button_yellow fs_12 bold c_blue2 pt_10 pb_10 pl_20 pr_20">Lanjut</button></div>
                          <input type="hidden" name="action" value="login" />
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php include('_footer.php'); ?>
      </div>
    </div>

    <?php include('_javascript.php'); ?>
  </body>
</html>